<?php
	class Abt_Model extends CI_Model {
		
		public function __construct() {
			parent::__construct();
		}
		
		public function get_data($name_preffix, $number, $offset) {
			return $query = $this->db->query("SELECT abt.*, satker.nama_satker, kegiatan.nama_kegiatan FROM abt INNER JOIN satker ON abt.kdsatker = satker.kode_satker INNER JOIN kegiatan ON abt.kdgiat = kegiatan.kode_kegiatan WHERE satker.nama_satker LIKE '%".$name_preffix."%' OR kegiatan.nama_kegiatan LIKE '%".$name_preffix."%' ORDER BY abt.kdsatker, abt.kdgiat ASC LIMIT $offset, $number;")->result();		
		}

		public function jumlah_data($name_preffix) {
			return $query = $this->db->query("SELECT COUNT(abt.id) AS jumlah FROM abt INNER JOIN satker ON abt.kdsatker = satker.kode_satker INNER JOIN kegiatan ON abt.kdgiat = kegiatan.kode_kegiatan WHERE satker.nama_satker LIKE '%".$name_preffix."%' OR kegiatan.nama_kegiatan LIKE '%".$name_preffix."%';")->row();
		}
		
	}
?>