<?php
	class Renstra_Model extends CI_Model {
		
		public function __construct() {
			parent::__construct();
		}
		
		public function get_data($name_preffix, $number, $offset) {
			return $query = $this->db->query("SELECT id, judul_file, path_file, is_highlight FROM renstra WHERE judul_file LIKE '%".$name_preffix."%' ORDER BY urutan ASC LIMIT $offset, $number;")->result();		
		}

		public function jumlah_data($name_preffix) {
			return $query = $this->db->query("SELECT COUNT(id) AS jumlah FROM renstra WHERE judul_file LIKE '%".$name_preffix."%';")->row();
		}

		public function get_data_by_id($id) {
			return $query = $this->db->query("SELECT id, judul_file, path_file, is_highlight FROM renstra WHERE id = '$id';")->row();
		}

		public function get_highlighted_row() {
			return $query = $this->db->query("SELECT id, judul_file, path_file, is_highlight FROM renstra WHERE is_highlight = '1';")->row();
		}
		
		public function tambah($judul_file, $path_file, $is_highlight, $urutan) {
			$data = array(
				'judul_file' => $judul_file,
				'path_file' => $path_file,
				'is_highlight' => $is_highlight,
				'urutan' => $urutan
				);
			$sql = $this->db->insert_string('renstra', $data);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function hapus($id) {
			if ($this->db->simple_query("DELETE FROM renstra WHERE id = '$id';")) {
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function check_highlight_exist() {
			$check = $this->db->query("SELECT id FROM renstra WHERE is_highlight = '1';");
			if ($check->num_rows() == 0) {
				return FALSE;
			} else {
				return TRUE;
			}
		}

		public function set_highlight($id) {
			$data = array(
				'is_highlight' => "1"
				);
			$where = "id = '$id'";
			
			$sql = $this->db->update_string('renstra', $data, $where);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function set_unhighlight($id) {
			$data = array(
				'is_highlight' => "0"
				);
			$where = "id = '$id'";
			
			$sql = $this->db->update_string('renstra', $data, $where);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}
		
	}
?>