<?php
	class Kegiatan_Model extends CI_Model {
		
		public function __construct() {
			parent::__construct();
		}

		public function get_data($name_preffix, $number, $offset) {
			return $query = $this->db->query("SELECT kode_kegiatan, nama_kegiatan FROM kegiatan WHERE nama_kegiatan LIKE '%".$name_preffix."%' OR kode_kegiatan LIKE '%".$name_preffix."%' ORDER BY kode_kegiatan ASC LIMIT $offset, $number;")->result();		
		}

		public function jumlah_data($name_preffix) {
			return $query = $this->db->query("SELECT COUNT(kode_kegiatan) AS jumlah FROM kegiatan WHERE nama_kegiatan LIKE '%".$name_preffix."%' OR kode_kegiatan LIKE '%".$name_preffix."%';")->row();
		}

		public function get_data_file($name_preffix, $number, $offset) {
			return $query = $this->db->query("SELECT file_kegiatan.*, satker.nama_satker, kegiatan.nama_kegiatan FROM file_kegiatan INNER JOIN satker ON file_kegiatan.kdsatker = satker.kode_satker INNER JOIN kegiatan ON file_kegiatan.kdgiat = kegiatan.kode_kegiatan WHERE satker.nama_satker LIKE '%".$name_preffix."%' OR kegiatan.nama_kegiatan LIKE '%".$name_preffix."%' ORDER BY file_kegiatan.kdsatker, file_kegiatan.kdgiat ASC LIMIT $offset, $number;")->result();	
		}

		public function jumlah_data_file($name_preffix) {
			return $query = $this->db->query("SELECT COUNT(file_kegiatan.id) AS jumlah FROM file_kegiatan INNER JOIN satker ON file_kegiatan.kdsatker = satker.kode_satker INNER JOIN kegiatan ON file_kegiatan.kdgiat = kegiatan.kode_kegiatan WHERE satker.nama_satker LIKE '%".$name_preffix."%' OR kegiatan.nama_kegiatan LIKE '%".$name_preffix."%';")->row();
		}

		public function get_file_kegiatan_by_id($id) {
			return $query = $this->db->query("SELECT file_kak_tor, file_rab, file_misc FROM file_kegiatan WHERE id = '$id';")->row();
		}

		public function unggah_file_kegiatan($id, $jenis, $filename) {
			if ($jenis == '1') {
				$data = array(
					'file_kak_tor' => $filename
				);
			} elseif ($jenis == '2') {
				$data = array(
					'file_rab' => $filename
				);
			} elseif ($jenis == '3') {
				$data = array(
					'file_misc' => $filename
				);
			}
			
			$where = "id = '$id'";
			
			$sql = $this->db->update_string('file_kegiatan', $data, $where);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function hapus_file_kegiatan($id, $jenis) {
			if ($jenis == '1') {
				$data = array(
					'file_kak_tor' => "-"
				);
			} elseif ($jenis == '2') {
				$data = array(
					'file_rab' => "-"
				);
			} elseif ($jenis == '3') {
				$data = array(
					'file_misc' => "-"
				);
			}
			
			$where = "id = '$id'";
			
			$sql = $this->db->update_string('file_kegiatan', $data, $where);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}
		
	}
?>