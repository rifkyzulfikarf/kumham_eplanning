<?php
	class Ankabut_Uke_Model extends CI_Model {
		
		public function __construct() {
			parent::__construct();
		}
		
		public function get_data($name_preffix, $number, $offset) {
			return $query = $this->db->query("SELECT usulan_ankabut_uke.*, satker.nama_satker FROM usulan_ankabut_uke INNER JOIN satker ON usulan_ankabut_uke.kdsatker = satker.kode_satker WHERE nama_satker LIKE '%".$name_preffix."%' ORDER BY usulan_ankabut_uke.id ASC LIMIT $offset, $number;")->result();
		}

		public function jumlah_data($name_preffix) {
			return $query = $this->db->query("SELECT COUNT(usulan_ankabut_uke.id) AS jumlah FROM usulan_ankabut_uke INNER JOIN satker ON usulan_ankabut_uke.kdsatker = satker.kode_satker WHERE nama_satker LIKE '%".$name_preffix."%';")->row();
		}

		public function get_file_by_id($id) {
			return $query = $this->db->query("SELECT file_kak_tor, file_rab, file_misc FROM usulan_ankabut_uke WHERE id = '$id';")->row();
		}
		
		public function tambah_usulan($kdsatker, $tahun, $ak_pagu, $ak_bp, $ak_bo, $ak_bbo, $ak_bm, $uke_pagu, $uke_bp, $uke_bo, $uke_bbo, $uke_bm) {
			$data = array(
				'kdsatker' => $kdsatker,
				'tahun' => $tahun,
				'ak_pagu' => $ak_pagu,
				'ak_bp' => $ak_bp,
				'ak_bo' => $ak_bo,
				'ak_bbo' => $ak_bbo,
				'ak_bm' => $ak_bm,
				'uke_pagu' => $uke_pagu,
				'uke_bp' => $uke_bp,
				'uke_bo' => $uke_bo,
				'uke_bbo' => $uke_bbo,
				'uke_bm' => $uke_bm,
				'file_kak_tor' => "-",
				'file_rab' => "-",
				'file_misc' => "-"
				);
			$sql = $this->db->insert_string('usulan_ankabut_uke', $data);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function hapus($id) {
			if ($this->db->simple_query("DELETE FROM usulan_ankabut_uke WHERE id = '$id';")) {
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function unggah_file($id, $jenis, $filename) {
			if ($jenis == '1') {
				$data = array(
					'file_kak_tor' => $filename
				);
			} elseif ($jenis == '2') {
				$data = array(
					'file_rab' => $filename
				);
			} elseif ($jenis == '3') {
				$data = array(
					'file_misc' => $filename
				);
			}
			
			$where = "id = '$id'";
			
			$sql = $this->db->update_string('usulan_ankabut_uke', $data, $where);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function hapus_file($id, $jenis) {
			if ($jenis == '1') {
				$data = array(
					'file_kak_tor' => "-"
				);
			} elseif ($jenis == '2') {
				$data = array(
					'file_rab' => "-"
				);
			} elseif ($jenis == '3') {
				$data = array(
					'file_misc' => "-"
				);
			}
			
			$where = "id = '$id'";
			
			$sql = $this->db->update_string('usulan_ankabut_uke', $data, $where);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}
		
	}
?>