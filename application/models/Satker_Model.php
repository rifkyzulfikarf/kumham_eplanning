<?php
	class Satker_Model extends CI_Model {
		
		public function __construct() {
			parent::__construct();
		}

		public function get_data($name_preffix, $number, $offset) {
			return $query = $this->db->query("SELECT kode_satker, nama_satker FROM satker WHERE nama_satker LIKE '%".$name_preffix."%' OR kode_satker LIKE '%".$name_preffix."%' ORDER BY kode_satker ASC LIMIT $offset, $number;")->result();		
		}

		public function jumlah_data($name_preffix) {
			return $query = $this->db->query("SELECT COUNT(kode_satker) AS jumlah FROM satker WHERE nama_satker LIKE '%".$name_preffix."%' OR kode_satker LIKE '%".$name_preffix."%';")->row();
		}
		
	}
?>