<?php
	class Sarpras_Model extends CI_Model {
		
		public function __construct() {
			parent::__construct();
		}
		
		public function get_data($name_preffix, $number, $offset) {
			return $query = $this->db->query("SELECT usulan_sarpras.*, satker.nama_satker, kegiatan.nama_kegiatan FROM usulan_sarpras INNER JOIN satker ON usulan_sarpras.kdsatker = satker.kode_satker INNER JOIN kegiatan ON usulan_sarpras.kdgiat = kegiatan.kode_kegiatan WHERE nama_satker LIKE '%".$name_preffix."%' OR nama_kegiatan LIKE '%".$name_preffix."%' ORDER BY usulan_sarpras.id ASC LIMIT $offset, $number;")->result();
		}

		public function jumlah_data($name_preffix) {
			return $query = $this->db->query("SELECT COUNT(usulan_sarpras.id) AS jumlah FROM usulan_sarpras INNER JOIN satker ON usulan_sarpras.kdsatker = satker.kode_satker INNER JOIN kegiatan ON usulan_sarpras.kdgiat = kegiatan.kode_kegiatan WHERE nama_satker LIKE '%".$name_preffix."%' OR nama_kegiatan LIKE '%".$name_preffix."%';")->row();
		}

		public function get_file_sarpras_by_id($id) {
			return $query = $this->db->query("SELECT file_kak_tor, file_rab, file_misc FROM usulan_sarpras WHERE id = '$id';")->row();
		}
		
		public function tambah_usulan($kdsatker, $kdgiat, $tahun, $ppdk, $pfk, $kb, $pb, $rehab, $renov, $pl, $satuan, $harga, $jumlah, $total) {
			$data = array(
				'kdsatker' => $kdsatker,
				'kdgiat' => $kdgiat,
				'tahun' => $tahun,
				'ppdk' => $ppdk,
				'pfk' => $pfk,
				'kb' => $kb,
				'pb' => $pb,
				'rehab' => $rehab,
				'renov' => $renov,
				'pl' => $pl,
				'satuan' => $satuan,
				'harga' => $harga,
				'jumlah' => $jumlah,
				'total' => $total,
				'file_kak_tor' => "-",
				'file_rab' => "-",
				'file_misc' => "-"
				);
			$sql = $this->db->insert_string('usulan_sarpras', $data);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function hapus($id) {
			if ($this->db->simple_query("DELETE FROM usulan_sarpras WHERE id = '$id';")) {
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function unggah_file_sarpras($id, $jenis, $filename) {
			if ($jenis == '1') {
				$data = array(
					'file_kak_tor' => $filename
				);
			} elseif ($jenis == '2') {
				$data = array(
					'file_rab' => $filename
				);
			} elseif ($jenis == '3') {
				$data = array(
					'file_misc' => $filename
				);
			}
			
			$where = "id = '$id'";
			
			$sql = $this->db->update_string('usulan_sarpras', $data, $where);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function hapus_file_sarpras($id, $jenis) {
			if ($jenis == '1') {
				$data = array(
					'file_kak_tor' => "-"
				);
			} elseif ($jenis == '2') {
				$data = array(
					'file_rab' => "-"
				);
			} elseif ($jenis == '3') {
				$data = array(
					'file_misc' => "-"
				);
			}
			
			$where = "id = '$id'";
			
			$sql = $this->db->update_string('usulan_sarpras', $data, $where);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}
		
	}
?>