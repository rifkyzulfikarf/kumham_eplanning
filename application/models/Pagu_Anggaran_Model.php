<?php
	class Pagu_Anggaran_Model extends CI_Model {
		
		public function __construct() {
			parent::__construct();
		}
		
		public function get_data($name_preffix, $number, $offset) {
			return $query = $this->db->query("SELECT pagu_anggaran.*, satker.nama_satker, kegiatan.nama_kegiatan FROM pagu_anggaran INNER JOIN satker ON pagu_anggaran.kdsatker = satker.kode_satker INNER JOIN kegiatan ON pagu_anggaran.kdgiat = kegiatan.kode_kegiatan WHERE satker.nama_satker LIKE '%".$name_preffix."%' OR kegiatan.nama_kegiatan LIKE '%".$name_preffix."%' ORDER BY pagu_anggaran.kdsatker, pagu_anggaran.kdgiat ASC LIMIT $offset, $number;")->result();		
		}

		public function jumlah_data($name_preffix) {
			return $query = $this->db->query("SELECT COUNT(pagu_anggaran.id) AS jumlah FROM pagu_anggaran INNER JOIN satker ON pagu_anggaran.kdsatker = satker.kode_satker INNER JOIN kegiatan ON pagu_anggaran.kdgiat = kegiatan.kode_kegiatan WHERE satker.nama_satker LIKE '%".$name_preffix."%' OR kegiatan.nama_kegiatan LIKE '%".$name_preffix."%';")->row();
		}

		public function get_data_by_id($id) {
			return $query = $this->db->query("SELECT id, judul_file, path_file, is_highlight FROM pn WHERE id = '$id';")->row();
		}

		public function get_highlighted_row() {
			return $query = $this->db->query("SELECT id, judul_file, path_file, is_highlight FROM pn WHERE is_highlight = '1';")->row();
		}
		
		public function tambah($judul_file, $path_file, $is_highlight, $urutan) {
			$data = array(
				'judul_file' => $judul_file,
				'path_file' => $path_file,
				'is_highlight' => $is_highlight,
				'urutan' => $urutan
				);
			$sql = $this->db->insert_string('pn', $data);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function hapus($id) {
			if ($this->db->simple_query("DELETE FROM pn WHERE id = '$id';")) {
				return TRUE;
			} else {
				return FALSE;
			};
		}
		
	}
?>