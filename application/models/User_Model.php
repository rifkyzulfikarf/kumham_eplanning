<?php
	class User_Model extends CI_Model {
		
		public function __construct() {
			parent::__construct();
		}
		
		public function check_login($username, $password) {
			$query_cek = $this->db->query("SELECT id, realname, avatar FROM user WHERE username = '$username' AND password = '".md5($password)."' AND is_active = '1';");
			if ($query_cek->num_rows() == 0) {
				return FALSE;
			} else {
				$user = $query_cek->row();

				$this->session->set_userdata('app-id', $user->id);
				$this->session->set_userdata('app-name', $user->realname);
				$this->session->set_userdata('app-avatar', $user->avatar);
				return TRUE;
			}
		}

		public function get_data($name_preffix, $number, $offset) {
			return $query = $this->db->query("SELECT users.id, users.nama, users.username, kotas.nama AS nama_kota, users.is_admin FROM users INNER JOIN kotas ON users.kota_id = kotas.id WHERE users.nama LIKE '%".$name_preffix."%' AND users.is_delete = '0' ORDER BY users.nama ASC LIMIT $offset, $number;")->result();		
		}

		public function jumlah_data($name_preffix) {
			return $query = $this->db->query("SELECT COUNT(users.id) AS jumlah FROM users INNER JOIN kotas ON users.kota_id = kotas.id WHERE users.nama LIKE '%".$name_preffix."%' AND users.is_delete = '0';")->row();
		}

		public function get_data_by_id($id) {
			return $query = $this->db->query("SELECT id, username, realname, password, last_login, row_inserted, row_updated FROM user WHERE id = '$id';")->row();
		}
		
		public function tambah($realname, $username, $password, $avatar) {
			$data = array(
				'nama' => $nama,
				'username' => $username,
				'password' => md5($password),
				'last_login' => '0',
				'kota_id' => $kota_id,
				'is_admin' => $is_admin,
				'is_delete' => "0"
				);
			$sql = $this->db->insert_string('users', $data);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function ubah($id, $realname, $username, $password, $avatar) {
			$data = array(
				'realname' => $realname,
				'username' => $username,
				'password' => md5($password),
				'avatar' => $avatar,
				'row_updated' => date('Y-m-d H:i:s')
				);
			$where = "id = '$id'";
			
			$sql = $this->db->update_string('user', $data, $where);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}

		public function nonaktif($id) {
			$data = array(
				'is_delete' => "1"
				);
			$where = "id = '$id'";
			
			$sql = $this->db->update_string('users', $data, $where);
			if ($this->db->simple_query($sql)) {
				return TRUE;
			} else {
				return FALSE;
			};
		}
		
	}
?>