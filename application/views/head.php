<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Pusat Data dan Informasi Kementerian Hukum dan HAM RI">
    <meta name="keyword" content="E-Planning">
    <link rel="shortcut icon" href="<?=base_url()?>assets/backend/img/favicon.png">

    <title>E-Planning Kementerian Hukum dan Hak Asasi Manusia</title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>assets/backend/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/backend/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="<?=base_url()?>assets/backend/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/backend/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" href="<?=base_url()?>assets/backend/css/owl.carousel.css" type="text/css">

    <!-- DataTables -->
    <link rel="stylesheet" href="<?=base_url()?>assets/backend/assets/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css">

    <!-- JQuery Chosen -->
    <link rel="stylesheet" href="<?=base_url()?>assets/backend/assets/jquery-chosen/chosen.css" ></script>

    <!--right slidebar-->
    <link href="<?=base_url()?>assets/backend/css/slidebars.css" rel="stylesheet">

    <!-- Custom styles for this template -->

    <link href="<?=base_url()?>assets/backend/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/backend/css/style-responsive.css" rel="stylesheet" />



    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>