<?php
    include(__DIR__ . "/head.php");
    include(__DIR__ . "/header.php");
?>
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <!--breadcrumbs start -->
                      <ul class="breadcrumb">
                          <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                          <li>Kebijakan</li>
                          <li class="active">RKP</li>
                      </ul>
                      <!--breadcrumbs end -->
                  </div>
              </div>
              <div class="row">
                  <div class="col-lg-8">
                    <section class="panel">
                        <header class="panel-heading" id="panel-heading">
                            <?=$highlight->judul_file?>
                        </header>
                        <div class="panel-body">
                            <embed src="<?=base_url()?>uploads/rkp/<?=$highlight->path_file?>" type="application/pdf" width="100%" height="750px" id="embed-pdf">
                        </div>
                    </section>
                  </div>
                  <div class="col-lg-4">
                    <section class="panel">
                        <header class="panel-heading">
                            File RKP Lainnya
                        </header>
                        <div class="list-group">
                            <?php
                              foreach ($rkps as $rkp) {
                            ?>
                                <a class="list-group-item pilih-pdf" href="#" data-judul="<?=$rkp->judul_file?>" data-file="<?=$rkp->path_file?>">
                                    <?=$rkp->judul_file?>
                                </a>
                            <?php
                              }
                            ?>
                        </div>
                    </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      
<?php
    include(__DIR__ . "/footer.php");
?>

  <script>
    $(function () {
        
        $('.pilih-pdf').click(function(ev){
            ev.preventDefault();
            var judul = $(this).data('judul');
            var file = $(this).data('file');
            $('#panel-heading').text(judul);
            $('#embed-pdf').attr('src', '<?=base_url()?>uploads/rkp/' + file);
        });

    });
  </script>

  </body>
</html>
