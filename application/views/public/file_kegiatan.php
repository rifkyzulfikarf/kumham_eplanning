<?php
    include(__DIR__ . "/head.php");
    include(__DIR__ . "/header.php");
?>
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <!--breadcrumbs start -->
                      <ul class="breadcrumb">
                          <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                      </ul>
                      <!--breadcrumbs end -->
                  </div>
              </div>

              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              File Kegiatan 2018
                          </header>
                          <div class="panel-body">
                              <table class="table table-bordered table-striped table-condensed table-kegiatan">
                                <thead>
                                  <tr>
                                      <th rowspan="2">Kode</th>
                                      <th rowspan="2">Satker</th>
                                      <th rowspan="2">Kegiatan</th>
                                      <th colspan="3">File Data Kegiatan</th>
                                  </tr>
                                  <tr>
                                      <th>KAK / TOR</th>
                                      <th>RAB</th>
                                      <th>Data Dukung Lainnya</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                    foreach ($files as $file) {
                                      if ($file->file_kak_tor != "-") {
                                        $kak_tor = "<a href='".base_url()."uploads/kak_tor_kegiatan/".$file->file_kak_tor."'>".$file->file_kak_tor."</a>";
                                      } else {
                                        $kak_tor = "-";
                                      }

                                      if ($file->file_rab != "-") {
                                        $rab = "<a href='".base_url()."uploads/rab_kegiatan/".$file->file_rab."'>".$file->file_rab."</a>";
                                      } else {
                                        $rab = "-";
                                      }

                                      if ($file->file_misc != "-") {
                                        $misc = "<a href='".base_url()."uploads/misc_kegiatan/".$file->file_misc."'>".$file->file_misc."</a>";
                                      } else {
                                        $misc = "-";
                                      }
                                  ?>
                                  <tr>
                                      <td><?=$file->kdsatker?></td>
                                      <td><?=$file->nama_satker?></td>
                                      <td><?=$file->nama_kegiatan?></td>
                                      <td><?=$kak_tor?></td>
                                      <td><?=$rab?></td>
                                      <td><?=$misc?></td>
                                  </tr>
                                  <?php
                                    }
                                  ?>
                                </tbody>
                              </table>
                          </div>
                      </section>
                  </div>
              </div>

              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      
<?php
    include(__DIR__ . "/footer.php");
?>

<script>
  $(function () {

    var tablekegiatan = $('.table-kegiatan').DataTable({
      "scrollX": true,
      "lengthMenu": [[-1], ["All"]],
      "paging": false,
      "ordering": false,
      "scrollY": "500px",
      "scrollCollapse": true,
      dom: 'Bfrtip',      
      buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
      ]
    });

  });
</script>

  </body>
</html>
