<?php
    include(__DIR__ . "/head.php");
    include(__DIR__ . "/header.php");
?>
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <!--breadcrumbs start -->
                      <ul class="breadcrumb">
                          <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                      </ul>
                      <!--breadcrumbs end -->
                  </div>
              </div>

              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Usulan Sarpras <?=$tahun?>
                          </header>
                          <div class="panel-body">
                              <div class="row">
                                <div class="col-lg-2">
                                  <select class="form-control" name="tahun" id="tahun">
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                  </select>
                                </div>
                                <div class="col-lg-10"></div>
                              </div>
                              <br>
                              <table class="table table-bordered table-striped table-condensed table-sarpras">
                                <thead>
                                  <tr>
                                      <th>Kode</th>
                                      <th>Satker</th>
                                      <th>Kegiatan</th>
                                      <th>Th Usulan</th>
                                      <th>Perangkat Pengolah Data dan Komunikasi</th>
                                      <th>Peralatan dan Fasilitas Perkantoran</th>
                                      <th>Kendaraan Bermotor</th>
                                      <th>Pembangunan Baru</th>
                                      <th>Rehab</th>
                                      <th>Renov</th>
                                      <th>Pembangunan Lanjutan</th>
                                      <th>Satuan</th>
                                      <th>Harga</th>
                                      <th>Jumlah</th>
                                      <th>Total</th>
                                      <th>KAK/TOR</th>
                                      <th>RAB</th>
                                      <th>File Lainnya</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                    $usulans = $this->db->query("SELECT usulan_sarpras.*, satker.nama_satker, kegiatan.nama_kegiatan FROM usulan_sarpras INNER JOIN satker ON usulan_sarpras.kdsatker = satker.kode_satker INNER JOIN kegiatan ON usulan_sarpras.kdgiat = kegiatan.kode_kegiatan WHERE usulan_sarpras.tahun = '$tahun';")->result();

                                    foreach ($usulans as $usulan) {
                                      $ppdk = ($usulan->ppdk == '1')?"&#10004;":"-";
                                      $pfk = ($usulan->pfk == '1')?"&#10004;":"-";
                                      $kb = ($usulan->kb == '1')?"&#10004;":"-";
                                      $pb = ($usulan->pb == '1')?"&#10004;":"-";
                                      $rehab = ($usulan->rehab == '1')?"&#10004;":"-";
                                      $renov = ($usulan->renov == '1')?"&#10004;":"-";
                                      $pl = ($usulan->pl == '1')?"&#10004;":"-";

                                      if ($usulan->file_kak_tor == '-') {
                                          $btn_kak_tor = "-";
                                      } else {
                                          $btn_kak_tor = "<a href='".base_url()."uploads/sarpras/kak_tor_kegiatan/".$usulan->file_kak_tor."' type='button' class='btn btn-info btn-xs tooltips btn-unduh' data-placement='top' data-toggle='tooltip' data-original-title='Unduh KAK / TOR'><i class='fa fa-download'></i></a>";
                                      }

                                      if ($usulan->file_rab == '-') {
                                          $btn_rab = "-";
                                      } else {
                                          $btn_rab = "<a href='".base_url()."uploads/sarpras/rab_kegiatan/".$usulan->file_rab."' type='button' class='btn btn-info btn-xs tooltips btn-unduh' data-placement='top' data-toggle='tooltip' data-original-title='Unduh RAB'><i class='fa fa-download'></i></a>";
                                      }

                                      if ($usulan->file_misc == '-') {
                                          $btn_misc = "-";
                                      } else {
                                          $btn_misc = "<a href='".base_url()."uploads/sarpras/misc_kegiatan/".$usulan->file_misc."' type='button' class='btn btn-info btn-xs tooltips btn-unduh' data-placement='top' data-toggle='tooltip' data-original-title='Unduh Data Dukung Lainnya'><i class='fa fa-download'></i></a>";
                                      }
                                  ?>
                                  <tr>
                                      <td><?=$usulan->kdsatker?></td>
                                      <td><?=$usulan->nama_satker?></td>
                                      <td><?=$usulan->nama_kegiatan?></td>
                                      <td><?=$usulan->tahun?></td>
                                      <td><?=$ppdk?></td>
                                      <td><?=$pfk?></td>
                                      <td><?=$kb?></td>
                                      <td><?=$pb?></td>
                                      <td><?=$rehab?></td>
                                      <td><?=$renov?></td>
                                      <td><?=$pl?></td>
                                      <td><?=$usulan->satuan?></td>
                                      <td><?=number_format($usulan->harga, 0, ',', '.')?></td>
                                      <td><?=number_format($usulan->jumlah, 0, ',', '.')?></td>
                                      <td><?=number_format($usulan->total, 0, ',', '.')?></td>
                                      <td><?=$btn_kak_tor?></td>
                                      <td><?=$btn_rab?></td>
                                      <td><?=$btn_misc?></td>
                                  </tr>
                                  <?php
                                    }
                                  ?>
                                </tbody>
                              </table>
                          </div>
                      </section>
                  </div>
              </div>

              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      
<?php
    include(__DIR__ . "/footer.php");
?>

<script>
  $(function () {

    $('#tahun').val('<?=$tahun?>');

    var tablesarpras = $('.table-sarpras').DataTable({
      "scrollX": true,
      "lengthMenu": [[-1], ["All"]],
      "paging": false,
      "ordering": false,
      "scrollY": "500px",
      "scrollCollapse": true,
      dom: 'Bfrtip',      
      buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
      ]
    });

    $('#tahun').change(function(ev){
      ev.preventDefault();
      var tahun = $('#tahun').val();
      var url = "<?=base_url()?>sarpras/frontend/" + tahun + "/";
      window.open(url,'_self');
    });

  });
</script>

  </body>
</html>
