<?php
    include(__DIR__ . "/head.php");
    include(__DIR__ . "/header.php");
?>
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <!--breadcrumbs start -->
                      <ul class="breadcrumb">
                          <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                      </ul>
                      <!--breadcrumbs end -->
                  </div>
              </div>

              <div class="row">
                  <div class="col-lg-12">
                    <section class="panel">
                        <div class="cover-photo">
                          <div class="fb-timeline-img">
                            <img src="<?=base_url()?>assets/backend/img/banner-e-planning.png" alt="">
                          </div>
                        </div>
                    </section>
                  </div>
              </div>

              <div class="row">
                  <div class="col-lg-8">
                    <section class="panel">
                      <header class="panel-heading tab-bg-dark-navy-blue">
                        <ul class="nav nav-tabs nav-justified ">
                          <li class="active">
                            <a href="#berita" data-toggle="tab" aria-expanded="true">
                            Berita Terkini
                            </a>
                          </li>
                          <li class="">
                            <a href="#site" data-toggle="tab" aria-expanded="false">
                            Situs Terkait
                            </a>
                          </li>
                        </ul>
                      </header>
                      <div class="panel-body">
                        <div class="tab-content tasi-tab">
                          <div class="tab-pane active" id="berita">
                            <article class="media">
                              <a class="pull-left thumb p-thumb">
                                <img src="<?=base_url()?>assets/backend/img/product1.jpg">
                              </a>
                              <div class="media-body">
                                <a class=" p-head" href="#">Item One Tittle</a>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                              </div>
                            </article>
                            <article class="media">
                              <a class="pull-left thumb p-thumb">
                                <img src="<?=base_url()?>assets/backend/img/product2.png">
                              </a>
                              <div class="media-body">
                                <a class=" p-head" href="#">Item Two Tittle</a>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                              </div>
                            </article>
                            <article class="media">
                              <a class="pull-left thumb p-thumb">
                                <img src="<?=base_url()?>assets/backend/img/product3.png">
                              </a>
                              <div class="media-body">
                                <a class=" p-head" href="#">Item Three Tittle</a>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                              </div>
                            </article>
                          </div>
                          <div class="tab-pane" id="site">
                            <article class="media">
                              <a class="pull-left thumb p-thumb">
                                <img src="<?=base_url()?>assets/backend/img/avatar-mini.jpg">
                              </a>
                              <div class="media-body">
                                <a class="cmt-head" href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a>
                                <p> <i class="fa fa-clock-o"></i> 1 hours ago</p>
                              </div>
                            </article>
                            <article class="media">
                              <a class="pull-left thumb p-thumb">
                                <img src="<?=base_url()?>assets/backend/img/avatar-mini2.jpg">
                              </a>
                              <div class="media-body">
                                <a class="cmt-head" href="#">Nulla vel metus scelerisque ante sollicitudin commodo</a>
                                <p> <i class="fa fa-clock-o"></i> 23 mins ago</p>
                              </div>
                            </article>
                            <article class="media">
                              <a class="pull-left thumb p-thumb">
                                <img src="<?=base_url()?>assets/backend/img/avatar-mini3.jpg">
                              </a>
                              <div class="media-body">
                                <a class="cmt-head" href="#">Donec lacinia congue felis in faucibus. </a>
                                <p> <i class="fa fa-clock-o"></i> 15 mins ago</p>
                              </div>
                            </article>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>

                  <div class="col-lg-4">
                    <div class="flat-carousal">
                      <div id="owl-demo" class="owl-carousel owl-theme">
                          <div class="item">
                              <h1><strong>Visi Kementerian Hukum dan Hak Asasi Manusia</strong></h1>
                              <h1>
                                  "Masyarakat memperoleh kepastian hukum".
                              </h1>
                          </div>
                          <div class="item">
                              <h1><strong>Misi Kementerian Hukum dan Hak Asasi Manusia</strong></h1>
                              <h1>
                                  Mewujudkan peraturan Perundang-Undangan yang berkualitas.
                              </h1>
                          </div>
                          <div class="item">
                              <h1><strong>Misi Kementerian Hukum dan Hak Asasi Manusia</strong></h1>
                              <h1>
                                  Mewujudkan pelayanan hukum yang berkualitas.
                              </h1>
                          </div>
                          <div class="item">
                              <h1><strong>Misi Kementerian Hukum dan Hak Asasi Manusia</strong></h1>
                              <h1>
                                  Mewujudkan penegakan hukum yang berkualitas.
                              </h1>
                          </div>
                          <div class="item">
                              <h1><strong>Misi Kementerian Hukum dan Hak Asasi Manusia</strong></h1>
                              <h1>
                                  Mewujudkan penghormatan, pemenuhan, dan perlindungan HAM.
                              </h1>
                          </div>
                          <div class="item">
                              <h1><strong>Misi Kementerian Hukum dan Hak Asasi Manusia</strong></h1>
                              <h1>
                                  Mewujudkan layanan manajemen administrasi  Kementerian Hukum dan HAM.
                              </h1>
                          </div>
                          <div class="item">
                              <h1><strong>Misi Kementerian Hukum dan Hak Asasi Manusia</strong></h1>
                              <h1>
                                  Mewujudkan aparatur Kementerian Hukum dan HAM  yang profesional dan berintegritas.
                              </h1>
                          </div>
                      </div>
                    </div>
                  </div>
              </div>

              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      
<?php
    include(__DIR__ . "/footer.php");
?>

  <script>
    $(function () {
        
      $("#owl-demo").owlCarousel({
        navigation : true,
        slideSpeed : 150,
        paginationSpeed : 200,
        singleItem : true,
        autoPlay:true

      });

    });
  </script>

  </body>
</html>
