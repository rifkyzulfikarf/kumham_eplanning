<?php
    include(__DIR__ . "/head.php");
    include(__DIR__ . "/header.php");
?>
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <!--breadcrumbs start -->
                      <ul class="breadcrumb">
                          <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                      </ul>
                      <!--breadcrumbs end -->
                  </div>
              </div>

              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Kegiatan APBNP
                          </header>
                          <div class="panel-body">
                              <table class="table table-bordered table-striped table-condensed table-apbnp">
                                <thead>
                                  <tr>
                                      <th rowspan="2">Kode</th>
                                      <th rowspan="2">Kegiatan</th>
                                      <th colspan="5">Pagu Alokasi</th>
                                      <th colspan="5">APBN-P</th>
                                  </tr>
                                  <tr>
                                      <th>Belanja Pegawai</th>
                                      <th>Belanja Barang Ops</th>
                                      <th>Belanja Barang Non Ops</th>
                                      <th>Belanja Modal</th>
                                      <th>Jumlah</th>
                                      <th>Belanja Pegawai</th>
                                      <th>Belanja Barang Ops</th>
                                      <th>Belanja Barang Non Ops</th>
                                      <th>Belanja Modal</th>
                                      <th>Jumlah</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                    foreach ($alokasis as $alokasi) {

                                      $apbnp = $this->db->query("SELECT apbnp.belanja_pegawai_ops, apbnp.belanja_barang_ops, apbnp.belanja_barang_nonops, apbnp.belanja_modal_nonops, apbnp.jumlah FROM apbnp WHERE kdgiat = '".$alokasi->kdgiat."' AND tahun = '2018';")->row();
                                  ?>
                                  <tr>
                                      <td><?=$alokasi->kdgiat?></td>
                                      <td><?=$alokasi->nama_kegiatan?></a></td>
                                      <td class="numeric"><?=number_format($alokasi->belanja_pegawai_ops, 0, ',', '.')?></td>
                                      <td class="numeric"><?=number_format($alokasi->belanja_barang_ops, 0, ',', '.')?></td>
                                      <td class="numeric"><?=number_format($alokasi->belanja_barang_nonops, 0, ',', '.')?></td>
                                      <td class="numeric"><?=number_format($alokasi->belanja_modal_nonops, 0, ',', '.')?></td>
                                      <td class="numeric"><?=number_format($alokasi->jumlah, 0, ',', '.')?></td>
                                      <td class="numeric"><?=number_format($apbnp->belanja_pegawai_ops, 0, ',', '.')?></td>
                                      <td class="numeric"><?=number_format($apbnp->belanja_barang_ops, 0, ',', '.')?></td>
                                      <td class="numeric"><?=number_format($apbnp->belanja_barang_nonops, 0, ',', '.')?></td>
                                      <td class="numeric"><?=number_format($apbnp->belanja_modal_nonops, 0, ',', '.')?></td>
                                      <td class="numeric"><?=number_format($apbnp->jumlah, 0, ',', '.')?></td>
                                  </tr>
                                  <?php
                                    }
                                  ?>
                                </tbody>
                              </table>
                          </div>
                      </section>
                  </div>
              </div>

              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      
<?php
    include(__DIR__ . "/footer.php");
?>

<script>
  $(function () {

    var tableapbnp = $('.table-apbnp').DataTable({
      "scrollX": true,
      "lengthMenu": [[-1], ["All"]],
      "paging": false,
      "ordering": false,
      "scrollY": "500px",
      "scrollCollapse": true,
      dom: 'Bfrtip',      
      buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
      ]
    });

  });
</script>

  </body>
</html>
