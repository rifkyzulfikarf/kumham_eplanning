<?php
    include(__DIR__ . "/head.php");
    include(__DIR__ . "/header.php");
?>
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <!--breadcrumbs start -->
                      <ul class="breadcrumb">
                          <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                      </ul>
                      <!--breadcrumbs end -->
                  </div>
              </div>

              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              ABT <?=$tahun?>
                          </header>
                          <div class="panel-body">
                              <div class="row">
                                <div class="col-lg-2">
                                  <select class="form-control" name="tahun" id="tahun">
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                  </select>
                                </div>
                                <div class="col-lg-10"></div>
                              </div>
                              <br>
                              <table class="table table-bordered table-striped table-condensed table-abt">
                                <thead>
                                  <tr>
                                      <th rowspan="2">Kode</th>
                                      <th rowspan="2">Satker</th>
                                      <th colspan="5">Pagu Alokasi</th>
                                      <th colspan="5">ABT</th>
                                  </tr>
                                  <tr>
                                      <th>Belanja Pegawai</th>
                                      <th>Belanja Barang Ops</th>
                                      <th>Belanja Barang Non Ops</th>
                                      <th>Belanja Modal</th>
                                      <th>Jumlah</th>
                                      <th>Belanja Pegawai</th>
                                      <th>Belanja Barang Ops</th>
                                      <th>Belanja Barang Non Ops</th>
                                      <th>Belanja Modal</th>
                                      <th>Jumlah</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                    foreach ($alokasis as $alokasi) {
                                      
                                      $abt = $this->db->query("SELECT SUM(abt.belanja_pegawai_ops) AS belanja_pegawai_ops, SUM(abt.belanja_barang_ops) AS belanja_barang_ops, SUM(abt.belanja_barang_nonops) AS belanja_barang_nonops, SUM(abt.belanja_modal_nonops) AS belanja_modal_nonops, SUM(abt.jumlah) AS jumlah FROM abt WHERE kdsatker = '".$alokasi->kdsatker."' AND tahun = '$tahun';")->row();
                                  ?>
                                  <tr>
                                      <td><?=$alokasi->kdsatker?></td>
                                      <td><a href="<?=base_url()?>abt/frontend_kegiatan/<?=$alokasi->kdsatker?>" target="blank"><?=$alokasi->nama_satker?></a></td>
                                      <td class="numeric"><?=number_format($alokasi->belanja_pegawai_ops, 0, ',', '.')?></td>
                                      <td class="numeric"><?=number_format($alokasi->belanja_barang_ops, 0, ',', '.')?></td>
                                      <td class="numeric"><?=number_format($alokasi->belanja_barang_nonops, 0, ',', '.')?></td>
                                      <td class="numeric"><?=number_format($alokasi->belanja_modal_nonops, 0, ',', '.')?></td>
                                      <td class="numeric"><?=number_format($alokasi->jumlah, 0, ',', '.')?></td>
                                      <td class="numeric"><?=number_format($abt->belanja_pegawai_ops, 0, ',', '.')?></td>
                                      <td class="numeric"><?=number_format($abt->belanja_barang_ops, 0, ',', '.')?></td>
                                      <td class="numeric"><?=number_format($abt->belanja_barang_nonops, 0, ',', '.')?></td>
                                      <td class="numeric"><?=number_format($abt->belanja_modal_nonops, 0, ',', '.')?></td>
                                      <td class="numeric"><?=number_format($abt->jumlah, 0, ',', '.')?></td>
                                  </tr>
                                  <?php
                                    }
                                  ?>
                                </tbody>
                              </table>
                          </div>
                      </section>
                  </div>
              </div>

              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      
<?php
    include(__DIR__ . "/footer.php");
?>

<script>
  $(function () {

    $('#tahun').val('<?=$tahun?>');

    var tableabt = $('.table-abt').DataTable({
      "scrollX": true,
      "lengthMenu": [[-1], ["All"]],
      "paging": false,
      "ordering": false,
      "scrollY": "500px",
      "scrollCollapse": true,
      dom: 'Bfrtip',      
      buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
      ]
    });

    $('#tahun').change(function(ev){
      ev.preventDefault();
      var tahun = $('#tahun').val();
      var url = "<?=base_url()?>abt/frontend/" + tahun + "/";
      window.open(url,'_self');
    });

  });
</script>

  </body>
</html>
