<?php
    include(__DIR__ . "/head.php");
    include(__DIR__ . "/header.php");
?>
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <!--breadcrumbs start -->
                      <ul class="breadcrumb">
                          <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                      </ul>
                      <!--breadcrumbs end -->
                  </div>
              </div>

              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Usulan Ankabut + UKE I <?=$tahun?>
                          </header>
                          <div class="panel-body">
                              <div class="row">
                                <div class="col-lg-2">
                                  <select class="form-control" name="tahun" id="tahun">
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                  </select>
                                </div>
                                <div class="col-lg-10"></div>
                              </div>
                              <br>
                              <table class="table table-bordered table-striped table-condensed table-ankabut">
                                <thead>
                                  <tr>
                                      <th rowspan="2">Kode</th>
                                      <th rowspan="2">Satker</th>
                                      <th rowspan="2">Th Usulan</th>
                                      <th colspan="5">Analisa Kebutuhan</th>
                                      <th colspan="5">Usulan UKE I</th>
                                      <th rowspan="2">KAK/TOR</th>
                                      <th rowspan="2">RAB</th>
                                      <th rowspan="2">File Lainnya</th>
                                  </tr>
                                  <tr>
                                      <th>Pagu</th>
                                      <th>BP</th>
                                      <th>BO</th>
                                      <th>BBO</th>
                                      <th>BM</th>
                                      <th>Pagu</th>
                                      <th>BP</th>
                                      <th>BO</th>
                                      <th>BBO</th>
                                      <th>BM</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                    $usulans = $this->db->query("SELECT usulan_ankabut_uke.*, satker.nama_satker FROM usulan_ankabut_uke INNER JOIN satker ON usulan_ankabut_uke.kdsatker = satker.kode_satker WHERE usulan_ankabut_uke.tahun = '$tahun';")->result();

                                    foreach ($usulans as $usulan) {
                                      
                                      if ($usulan->file_kak_tor == '-') {
                                          $btn_kak_tor = "-";
                                      } else {
                                          $btn_kak_tor = "<a href='".base_url()."uploads/ankabut_uke/kak_tor_kegiatan/".$usulan->file_kak_tor."' type='button' class='btn btn-info btn-xs tooltips btn-unduh' data-placement='top' data-toggle='tooltip' data-original-title='Unduh KAK / TOR'><i class='fa fa-download'></i></a>";
                                      }

                                      if ($usulan->file_rab == '-') {
                                          $btn_rab = "-";
                                      } else {
                                          $btn_rab = "<a href='".base_url()."uploads/ankabut_uke/rab_kegiatan/".$usulan->file_rab."' type='button' class='btn btn-info btn-xs tooltips btn-unduh' data-placement='top' data-toggle='tooltip' data-original-title='Unduh RAB'><i class='fa fa-download'></i></a>";
                                      }

                                      if ($usulan->file_misc == '-') {
                                          $btn_misc = "-";
                                      } else {
                                          $btn_misc = "<a href='".base_url()."uploads/ankabut_uke/misc_kegiatan/".$usulan->file_misc."' type='button' class='btn btn-info btn-xs tooltips btn-unduh' data-placement='top' data-toggle='tooltip' data-original-title='Unduh Data Dukung Lainnya'><i class='fa fa-download'></i></a>";
                                      }
                                  ?>
                                  <tr>
                                      <td><?=$usulan->kdsatker?></td>
                                      <td><?=$usulan->nama_satker?></td>
                                      <td><?=$usulan->tahun?></td>
                                      <td><?=number_format($usulan->ak_pagu, 0, ',', '.')?></td>
                                      <td><?=number_format($usulan->ak_bp, 0, ',', '.')?></td>
                                      <td><?=number_format($usulan->ak_bo, 0, ',', '.')?></td>
                                      <td><?=number_format($usulan->ak_bbo, 0, ',', '.')?></td>
                                      <td><?=number_format($usulan->ak_bm, 0, ',', '.')?></td>
                                      <td><?=number_format($usulan->uke_pagu, 0, ',', '.')?></td>
                                      <td><?=number_format($usulan->uke_bp, 0, ',', '.')?></td>
                                      <td><?=number_format($usulan->uke_bo, 0, ',', '.')?></td>
                                      <td><?=number_format($usulan->uke_bbo, 0, ',', '.')?></td>
                                      <td><?=number_format($usulan->uke_bm, 0, ',', '.')?></td>
                                      <td><?=$btn_kak_tor?></td>
                                      <td><?=$btn_rab?></td>
                                      <td><?=$btn_misc?></td>
                                  </tr>
                                  <?php
                                    }
                                  ?>
                                </tbody>
                              </table>
                          </div>
                      </section>
                  </div>
              </div>

              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      
<?php
    include(__DIR__ . "/footer.php");
?>

<script>
  $(function () {

    $('#tahun').val('<?=$tahun?>');

    var tableankabut = $('.table-ankabut').DataTable({
      "scrollX": true,
      "lengthMenu": [[-1], ["All"]],
      "paging": false,
      "ordering": false,
      "scrollY": "500px",
      "scrollCollapse": true,
      dom: 'Bfrtip',      
      buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
      ]
    });

    $('#tahun').change(function(ev){
      ev.preventDefault();
      var tahun = $('#tahun').val();
      var url = "<?=base_url()?>ankabut_uke/frontend/" + tahun + "/";
      window.open(url,'_self');
    });

  });
</script>

  </body>
</html>
