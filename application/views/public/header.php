<body class="full-width">

  <section id="container" class="">
      <!--header start-->
      <header class="header white-bg">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="fa fa-bars"></span>
              </button>

              <!--logo start-->
              <a href="<?=base_url()?>" class="logo" >E-<span>Planning</span></a>
              <!--logo end-->
              <div class="horizontal-menu navbar-collapse collapse ">
                  <ul class="nav navbar-nav">
                      <li class="active"><a href="<?=base_url()?>">Beranda</a></li>
                      <li class="dropdown">
                          <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#">Kebijakan <b class=" fa fa-angle-down"></b></a>
                          <ul class="dropdown-menu">
                              <li><a href="<?=base_url()?>rpjmn/frontend/">RPJMN</a></li>
                              <li><a href="<?=base_url()?>iku/frontend/">Indikator Kinerja Utama</a></li>
                              <li><a href="<?=base_url()?>renstra/frontend/">Renstra</a></li>
                              <li><a href="<?=base_url()?>rkp/frontend/">RKP</a></li>
                              <li><a href="<?=base_url()?>pn/frontend/">Prioritas Nasional</a></li>
                              <li><a href="<?=base_url()?>tm/frontend/">Trilateral Meeting</a></li>
                          </ul>
                      </li>
                      <li class="dropdown">
                          <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#">Pagu <b class=" fa fa-angle-down"></b></a>
                          <ul class="dropdown-menu">
                              <li><a href="<?=base_url()?>home/pagu/">Pagu</a></li>
                              <li><a href="http://krisna.bappenas.go.id" target="_blank">Renja</a></li>
                              <li><a href="<?=base_url()?>apbnp/frontend/">APBN-P</a></li>
                              <li><a href="<?=base_url()?>abt/frontend/">ABT</a></li>
                          </ul>
                      </li>
                      <li class="dropdown">
                          <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#">Usulan <b class=" fa fa-angle-down"></b></a>
                          <ul class="dropdown-menu">
                              <li><a href="<?=base_url()?>sarpras/frontend/">Sarpras</a></li>
                              <li><a href="<?=base_url()?>ankabut_uke/frontend/">Analisa Kebutuhan + UKE I</a></li>
                          </ul>
                      </li>
                      <li><a href="<?=base_url()?>kegiatan/frontend/">File Data Kegiatan</a></li>
                  </ul>

              </div>
              <div class="top-nav ">
                  <ul class="nav pull-right top-menu">
                      <li>
                        <a class="dropdown-toggle" href="<?=base_url()?>user/" target="_blank">
                            <img alt="" src="<?=base_url()?>assets/backend/img/avatar_logout.png">
                        </a>
                      </li>
                  </ul>
              </div>

          </div>

      </header>