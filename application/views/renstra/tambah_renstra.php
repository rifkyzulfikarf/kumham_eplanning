<?php
include(__DIR__ . "/../head.php");
include(__DIR__ . "/../header.php");
include(__DIR__ . "/../sidebar.php");
?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            
            <?php if (isset($info)) { ?>
            <div class="alert <?=$info_type?> fade in">
                <button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>
                <?=$info_pesan?>
            </div>
            <?php } ?>

            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <section class="panel">
                        <header class="panel-heading">Unggah File Renstra</header>
                        <div class="panel-body">
                            <?=form_open('renstra/tambah/', 'autocomplete="off" class="form-horizontal" role="form" enctype="multipart/form-data"'); ?>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Judul File</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" placeholder="Masukkan judul file..." name="judul">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label"></label>
                                    <div class="col-lg-10">
                                        <input type="file" id="file" name="file">
                                        <p class="help-block">Unggah hanya dokumen berformat .pdf</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button type="submit" class="btn btn-primary" name="btnSubmit">Unggah File <i class="fa fa-upload"></i></button>
                                    </div>
                                </div>
                            <?=form_close(); ?>
                        </div>
                    </section>
                </div>
                <div class="col-lg-2"></div>
            </div>
        </section>
    </section>
    <!--main content end-->

<?php
include(__DIR__ . "/../footer.php");
?>

</body>
</html>
