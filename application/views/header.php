<body>

  <section id="container" class="sidebar-closed">
      <!--header start-->
      <header class="header blue-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="<?=base_url()?>user/" class="logo">E-<span>Planning</span></a>
            <!--logo end-->
            <div class="top-nav ">
                <!--search & user info start-->
                <ul class="nav pull-right top-menu">
                    <li>
                        <input type="text" class="form-control search" placeholder="Search">
                    </li>
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <img alt="" src="<?=base_url()?>assets/backend/img/<?=$this->session->userdata('app-avatar')?>">
                            <span class="username"><?=$this->session->userdata('app-name')?></span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li><a href="<?=base_url()?>user/profil_saya/"><i class="fa fa-user"></i> Profil</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="dropdown-toggle" href="<?=base_url()?>user/logout/">
                            <img alt="" src="<?=base_url()?>assets/backend/img/avatar_logout.png">
                        </a>
                    </li>
                </ul>
                <!--search & user info end-->
            </div>
        </header>
      <!--header end-->