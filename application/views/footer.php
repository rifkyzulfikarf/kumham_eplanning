      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              &copy;2018 Pusat Data dan Informasi Kementerian Hukum dan Hak Asasi Manusia
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?=base_url()?>assets/backend/js/jquery.js"></script>
    <script src="<?=base_url()?>assets/backend/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?=base_url()?>assets/backend/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?=base_url()?>assets/backend/js/jquery.scrollTo.min.js"></script>
    <script src="<?=base_url()?>assets/backend/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?=base_url()?>assets/backend/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="<?=base_url()?>assets/backend/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="<?=base_url()?>assets/backend/js/owl.carousel.js" ></script>
    <script src="<?=base_url()?>assets/backend/js/jquery.customSelect.min.js" ></script>
    <script src="<?=base_url()?>assets/backend/js/respond.min.js" ></script>

    <!-- DataTables -->
    <script src="<?=base_url()?>assets/backend/assets/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>assets/backend/assets/datatables/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>

    <!-- JQuery Chosen -->
    <script src="<?=base_url()?>assets/backend/assets/jquery-chosen/chosen.jquery.min.js" ></script>

    <!--right slidebar-->
    <script src="<?=base_url()?>assets/backend/js/slidebars.min.js"></script>

    <!--common script for all pages-->
    <script src="<?=base_url()?>assets/backend/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="<?=base_url()?>assets/backend/js/sparkline-chart.js"></script>
    <script src="<?=base_url()?>assets/backend/js/easy-pie-chart.js"></script>
    <script src="<?=base_url()?>assets/backend/js/count.js"></script>

    <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
    		  autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

      $('.chosen-select').chosen();

    </script>