<!--sidebar start-->
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <li><a class="active" href="<?=base_url()?>user/"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

            <li class="sub-menu">
                <a href="javascript:;" >
                    <i class="fa fa-gavel"></i>
                    <span>Kebijakan</span>
                </a>
                <ul class="sub">
                    <li><a href="<?=base_url()?>rpjmn/master/">RPJMN</a></li>
                    <li><a href="<?=base_url()?>iku/master/">IKU</a></li>
                    <li><a href="<?=base_url()?>renstra/master/">Renstra</a></li>
                    <li><a href="<?=base_url()?>rkp/master/">RKP</a></li>
                    <li><a href="<?=base_url()?>pn/master/">Prioritas Nasional</a></li>
                    <li><a href="<?=base_url()?>tm/master/">Trilateral Meeting</a></li>
                </ul>
            </li>

            <li class="sub-menu">
                <a href="javascript:;" >
                    <i class="fa fa-level-up"></i>
                    <span>Pagu</span>
                </a>
                <ul class="sub">
                    <li><a href="<?=base_url()?>pagu_indikatif/master/">Pagu Indikatif</a></li>
                    <li><a href="<?=base_url()?>pagu_anggaran/master/">Pagu Anggaran</a></li>
                    <li><a href="<?=base_url()?>pagu_alokasi/master/">Pagu Alokasi Anggaran</a></li>
                    <li><a href="<?=base_url()?>apbnp/master/">APBN-P</a></li>
                    <li><a href="<?=base_url()?>abt/master/">ABT</a></li>
                </ul>
            </li>

            <li class="sub-menu">
                <a href="javascript:;" >
                    <i class="fa fa-retweet"></i>
                    <span>Usulan</span>
                </a>
                <ul class="sub">
                    <li><a href="<?=base_url()?>sarpras/usulan/">Sarana Prasarana</a></li>
                    <li><a href="<?=base_url()?>ankabut_uke/usulan/">Analisa Kebutuhan + UKE I</a></li>
                </ul>
            </li>

            <li><a href="<?=base_url()?>kegiatan/unggah_file/"><i class="fa fa-upload"></i> <span>Unggah File Kegiatan</span></a></li>

            <li class="sub-menu">
                <a href="javascript:;" >
                    <i class="fa fa-globe"></i>
                    <span>Master Data</span>
                </a>
                <ul class="sub">
                    <li><a href="<?=base_url()?>satker/master/">Satuan Kerja</a></li>
                    <li><a href="<?=base_url()?>kegiatan/master/">Kegiatan</a></li>
                    <li><a href="<?=base_url()?>user/">Pengguna Aplikasi</a></li>
                </ul>
            </li>

        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->