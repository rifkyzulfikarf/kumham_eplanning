<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Pusat Data dan Informasi Kementerian Hukum dan HAM RI">
    <meta name="keyword" content="E-Planning">
    <link rel="shortcut icon" href="<?=base_url()?>assets/backend/img/favicon.png">

    <title>E-Planning Kementerian Hukum dan Hak Asasi Manusia</title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>assets/backend/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/backend/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="<?=base_url()?>assets/backend/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="<?=base_url()?>assets/backend/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/backend/css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

  <body class="login-body">

    <div class="container">

      <?=form_open('user/login/', 'class="form-signin"'); ?>
        <h2 class="form-signin-heading">e-planning kemenkumham</h2>
        <div class="login-wrap">
            <?php if (isset($pesan)) { echo $pesan; } ?>
            <input type="text" class="form-control" placeholder="Username" autofocus name="username">
            <input type="password" class="form-control" placeholder="Password" name="password">
            <button class="btn btn-lg btn-login btn-block" type="submit">Sign in</button>
        </div>
      <?=form_close(); ?>

    </div>



    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?=base_url()?>assets/backend/js/jquery.js"></script>
    <script src="<?=base_url()?>assets/backend/js/bootstrap.min.js"></script>


  </body>
</html>
