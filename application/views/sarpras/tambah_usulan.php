<?php
include(__DIR__ . "/../head.php");
include(__DIR__ . "/../header.php");
include(__DIR__ . "/../sidebar.php");
?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            
            <?php if (isset($info)) { ?>
            <div class="alert <?=$info_type?> fade in">
                <button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>
                <?=$info_pesan?>
            </div>
            <?php } ?>

            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <section class="panel">
                        <header class="panel-heading">Tambah Usulan Sarana Prasana</header>
                        <div class="panel-body">
                            <?=form_open('sarpras/tambah_usulan/', 'autocomplete="off" class="form-horizontal" role="form"'); ?>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Satker</label>
                                    <div class="col-lg-10">
                                        <select class="form-control chosen-select" name="satker">
                                            <?php
                                                $query = $this->db->query("SELECT kode_satker, nama_satker FROM satker");
                                                foreach ($query->result() as $row) {
                                                    echo "<option value='".$row->kode_satker."'>".$row->nama_satker."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Kegiatan</label>
                                    <div class="col-lg-10">
                                        <select class="form-control chosen-select" name="kegiatan">
                                            <?php
                                                $query = $this->db->query("SELECT kode_kegiatan, nama_kegiatan FROM kegiatan");
                                                foreach ($query->result() as $row) {
                                                    echo "<option value='".$row->kode_kegiatan."'>".$row->nama_kegiatan."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Tahun</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="tahun">
                                            <option value='2018'>2018</option>
                                            <option value='2019'>2019</option>
                                            <option value='2020'>2020</option>
                                            <option value='2021'>2021</option>
                                            <option value='2022'>2022</option>
                                        </select>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-lg-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="ppdk" value="1"> Perangkat Pengolah Data dan Komunikasi
                                        </label>
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="pfk" value="1"> Peralatan dan Fasilitas Perkantoran
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="kb" value="1"> Kendaraan Bermotor
                                        </label>
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="pb" value="1"> Pembangunan Baru
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="rehab" value="1"> Rehabilitasi
                                        </label>
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="renov" value="1"> Renovasi
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="pl" value="1"> Pembangunan Lanjutan
                                        </label>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Satuan</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="satuan">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Harga</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="harga">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Jumlah</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="jumlah">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Total</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="total">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button type="submit" class="btn btn-primary" name="btnSubmit">Simpan Usulan <i class="fa fa-share"></i></button>
                                    </div>
                                </div>
                            <?=form_close(); ?>
                        </div>
                    </section>
                </div>
                <div class="col-lg-2"></div>
            </div>
        </section>
    </section>
    <!--main content end-->

<?php
include(__DIR__ . "/../footer.php");
?>

<script>
    $(function () {

        

    });
</script>

</body>
</html>
