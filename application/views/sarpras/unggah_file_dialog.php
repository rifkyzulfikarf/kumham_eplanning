<?php
include(__DIR__ . "/../head.php");
include(__DIR__ . "/../header.php");
include(__DIR__ . "/../sidebar.php");

if (base64_decode($jenis) == '1') {
    $nama_jenis = 'KAK / TOR';
} elseif(base64_decode($jenis) == '2') {
    $nama_jenis = 'RAB';
} elseif(base64_decode($jenis) == '3') {
    $nama_jenis = 'Data Dukung Lainnya';
}
?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            
            <?php if (isset($info)) { ?>
            <div class="alert <?=$info_type?> fade in">
                <button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>
                <?=$info_pesan?>
            </div>
            <?php } ?>

            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <section class="panel">
                        <header class="panel-heading">Unggah File Sarpras (<?=$nama_jenis?>)</header>
                        <div class="panel-body">
                            <?=form_open('sarpras/unggah_file_dialog/', 'autocomplete="off" class="form-horizontal" role="form" enctype="multipart/form-data"'); ?>
                                <input type="hidden" name="id" value="<?=$id?>">
                                <input type="hidden" name="jenis" value="<?=$jenis?>">
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label"></label>
                                    <div class="col-lg-10">
                                        <input type="file" id="file" name="file">
                                        <p class="help-block">Unggah hanya dokumen berformat .pdf</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button type="submit" class="btn btn-primary" name="btnSubmit">Unggah File <i class="fa fa-upload"></i></button>
                                    </div>
                                </div>
                            <?=form_close(); ?>
                        </div>
                    </section>
                </div>
                <div class="col-lg-2"></div>
            </div>
        </section>
    </section>
    <!--main content end-->

<?php
include(__DIR__ . "/../footer.php");
?>

</body>
</html>
