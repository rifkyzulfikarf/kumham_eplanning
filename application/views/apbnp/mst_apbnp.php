<?php
include(__DIR__ . "/../head.php");
include(__DIR__ . "/../header.php");
include(__DIR__ . "/../sidebar.php");
?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            
            <?php if (isset($info)) { ?>
            <div class="alert <?=$info_type?> fade in">
                <button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>
                <?=$info_pesan?>
            </div>
            <?php } ?>

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            APBNP-P
                        </header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="Masukkan nama satuan kerja atau nama kegiatan yang akan dicari.." name="keyword" id="keyword">
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary btn-sm" id="btn-cari"><i class="fa fa-search"></i> Cari Data</button>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="table-apbnp" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Kode Satker</th>
                                                <th>Nama Satker</th>
                                                <th>Nama Kegiatan</th>
                                                <th>Belanja Pegawai</th>
                                                <th>Belanja Barang Ops</th>
                                                <th>Belanja Barang Non Ops</th>
                                                <th>Belanja Modal</th>
                                                <th>Jumlah</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            $no = $this->uri->segment(4, 0);
                                            foreach($apbnps as $apbnp) {
                                                $no++;
                                            ?>
                                            <tr>
                                                <td><?=$no?></td>
                                                <td><?=$apbnp->kdsatker?></td>
                                                <td><?=$apbnp->nama_satker?></td>
                                                <td><?=$apbnp->nama_kegiatan?></td>
                                                <td><?=number_format($apbnp->belanja_pegawai_ops, 0, ',', '.')?></td>
                                                <td><?=number_format($apbnp->belanja_barang_ops, 0, ',', '.')?></td>
                                                <td><?=number_format($apbnp->belanja_barang_nonops, 0, ',', '.')?></td>
                                                <td><?=number_format($apbnp->belanja_modal_nonops, 0, ',', '.')?></td>
                                                <td><?=number_format($apbnp->jumlah, 0, ',', '.')?></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <ul class="pagination pull-right">
                                        <?=$this->pagination->create_links();?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
    <!--main content end-->

<?php
include(__DIR__ . "/../footer.php");
?>

<script>
    $(function () {
        
        <?php if ($keyword != "ALL") { ?>
        $('#keyword').val("<?=$keyword?>");
        <?php } ?>

        $('#btn-cari').click(function(ev){
            ev.preventDefault();
            if ($('#keyword').val() != "") {
                var keyword = $('#keyword').val();
            } else {
                var keyword = "all";
            }
            var url = "<?=base_url()?>apbnp/master/" + keyword + "/";
            window.open(url,'_self');
        });

    });
</script>

</body>
</html>
