<?php
include(__DIR__ . "/../head.php");
include(__DIR__ . "/../header.php");
include(__DIR__ . "/../sidebar.php");
?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            
            <?php if (isset($info)) { ?>
            <div class="alert <?=$info_type?> fade in">
                <button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>
                <?=$info_pesan?>
            </div>
            <?php } ?>

            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <section class="panel">
                        <header class="panel-heading">Tambah Analisa Kebutuhan dan Usulan UKE I</header>
                        <div class="panel-body">
                            <?=form_open('ankabut_uke/tambah_usulan/', 'autocomplete="off" class="form-horizontal" role="form"'); ?>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Satker</label>
                                    <div class="col-lg-10">
                                        <select class="form-control chosen-select" name="satker">
                                            <?php
                                                $query = $this->db->query("SELECT kode_satker, nama_satker FROM satker");
                                                foreach ($query->result() as $row) {
                                                    echo "<option value='".$row->kode_satker."'>".$row->nama_satker."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Tahun</label>
                                    <div class="col-lg-10">
                                        <select class="form-control" name="tahun">
                                            <option value='2018'>2018</option>
                                            <option value='2019'>2019</option>
                                            <option value='2020'>2020</option>
                                            <option value='2021'>2021</option>
                                            <option value='2022'>2022</option>
                                        </select>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h4>Analisa Kebutuhan</h4>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Pagu</label>
                                            <div class="col-lg-8 input-group m-bot15">
                                                <span class="input-group-addon">Rp</span>
                                                <input type="text" class="form-control" name="ak_pagu">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">BP</label>
                                            <div class="col-lg-8 input-group m-bot15">
                                                <span class="input-group-addon">Rp</span>
                                                <input type="text" class="form-control" name="ak_bp">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">BO</label>
                                            <div class="col-lg-8 input-group m-bot15">
                                                <span class="input-group-addon">Rp</span>
                                                <input type="text" class="form-control" name="ak_bo">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">BBO</label>
                                            <div class="col-lg-8 input-group m-bot15">
                                                <span class="input-group-addon">Rp</span>
                                                <input type="text" class="form-control" name="ak_bbo">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">BM</label>
                                            <div class="col-lg-8 input-group m-bot15">
                                                <span class="input-group-addon">Rp</span>
                                                <input type="text" class="form-control" name="ak_bm">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <h4>Usulan UKE I</h4>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Pagu</label>
                                            <div class="col-lg-8 input-group m-bot15">
                                                <span class="input-group-addon">Rp</span>
                                                <input type="text" class="form-control" name="uke_pagu">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">BP</label>
                                            <div class="col-lg-8 input-group m-bot15">
                                                <span class="input-group-addon">Rp</span>
                                                <input type="text" class="form-control" name="uke_bp">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">BO</label>
                                            <div class="col-lg-8 input-group m-bot15">
                                                <span class="input-group-addon">Rp</span>
                                                <input type="text" class="form-control" name="uke_bo">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">BBO</label>
                                            <div class="col-lg-8 input-group m-bot15">
                                                <span class="input-group-addon">Rp</span>
                                                <input type="text" class="form-control" name="uke_bbo">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">BM</label>
                                            <div class="col-lg-8 input-group m-bot15">
                                                <span class="input-group-addon">Rp</span>
                                                <input type="text" class="form-control" name="uke_bm">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <button type="submit" class="btn btn-primary pull-right" name="btnSubmit">Simpan Usulan <i class="fa fa-share"></i></button>
                                    </div>
                                </div>
                            <?=form_close(); ?>
                        </div>
                    </section>
                </div>
                <div class="col-lg-2"></div>
            </div>
        </section>
    </section>
    <!--main content end-->

<?php
include(__DIR__ . "/../footer.php");
?>

<script>
    $(function () {

        

    });
</script>

</body>
</html>
