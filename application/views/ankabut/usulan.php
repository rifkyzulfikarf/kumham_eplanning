<?php
include(__DIR__ . "/../head.php");
include(__DIR__ . "/../header.php");
include(__DIR__ . "/../sidebar.php");
?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            
            <?php if (isset($info)) { ?>
            <div class="alert <?=$info_type?> fade in">
                <button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>
                <?=$info_pesan?>
            </div>
            <?php } ?>

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Data Analisa Kebutuhan + Usulan UKE I
                            <a href="<?=base_url()?>ankabut_uke/tambah_usulan/" type="button" class="btn btn-info btn-sm pull-right"><i class="fa fa-plus"></i> Tambah Usulan</a>
                        </header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="Masukkan nama kegiatan atau satker yang akan dicari.." name="keyword" id="keyword">
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary btn-sm" id="btn-cari"><i class="fa fa-search"></i> Cari Data</button>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="table-usulan" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">Aksi</th>
                                                <th rowspan="2">Kode</th>
                                                <th rowspan="2">Satker</th>
                                                <th rowspan="2">Th Usulan</th>
                                                <th colspan="5">Analisa Kebutuhan</th>
                                                <th colspan="5">Usulan UKE I</th>
                                                <th rowspan="2">KAK/TOR</th>
                                                <th rowspan="2">RAB</th>
                                                <th rowspan="2">File Lainnya</th>
                                            </tr>
                                            <tr>
                                                <th>Pagu</th>
                                                <th>BP</th>
                                                <th>BO</th>
                                                <th>BBO</th>
                                                <th>BM</th>
                                                <th>Pagu</th>
                                                <th>BP</th>
                                                <th>BO</th>
                                                <th>BBO</th>
                                                <th>BM</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            $no = $this->uri->segment(4, 0);
                                            foreach($usulans as $usulan) {
                                                $no++;

                                                if ($usulan->file_kak_tor == '-') {
                                                    $btn_kak_tor = "<a href='".base_url()."ankabut_uke/unggah_file_dialog/1/".$usulan->id."' type='button' class='btn btn-info btn-xs tooltips btn-unggah' data-placement='top' data-toggle='tooltip' data-original-title='Unggah KAK / TOR'><i class='fa fa-upload'></i></a>";
                                                } else {
                                                    $btn_kak_tor = "<a href='".base_url()."uploads/ankabut_uke/kak_tor_kegiatan/".$usulan->file_kak_tor."' type='button' class='btn btn-info btn-xs tooltips btn-unduh' data-placement='top' data-toggle='tooltip' data-original-title='Unduh KAK / TOR'><i class='fa fa-download'></i></a>";
                                                    $btn_kak_tor .= " <a href='".base_url()."ankabut_uke/hapus_file/1/".$usulan->id."' type='button' class='btn btn-danger btn-xs tooltips btn-hapus-file' data-placement='top' data-toggle='tooltip' data-original-title='Hapus KAK / TOR'><i class='fa fa-trash'></i></a>";
                                                }

                                                if ($usulan->file_rab == '-') {
                                                    $btn_rab = "<a href='".base_url()."ankabut_uke/unggah_file_dialog/2/".$usulan->id."' type='button' class='btn btn-info btn-xs tooltips btn-unggah' data-placement='top' data-toggle='tooltip' data-original-title='Unggah RAB'><i class='fa fa-upload'></i></a>";
                                                } else {
                                                    $btn_rab = "<a href='".base_url()."uploads/ankabut_uke/rab_kegiatan/".$usulan->file_rab."' type='button' class='btn btn-info btn-xs tooltips btn-unduh' data-placement='top' data-toggle='tooltip' data-original-title='Unduh RAB'><i class='fa fa-download'></i></a>";
                                                    $btn_rab .= " <a href='".base_url()."ankabut_uke/hapus_file/2/".$usulan->id."' type='button' class='btn btn-danger btn-xs tooltips btn-hapus-file' data-placement='top' data-toggle='tooltip' data-original-title='Hapus RAB'><i class='fa fa-trash'></i></a>";
                                                }

                                                if ($usulan->file_misc == '-') {
                                                    $btn_misc = "<a href='".base_url()."ankabut_uke/unggah_file_dialog/3/".$usulan->id."' type='button' class='btn btn-info btn-xs tooltips btn-unggah' data-placement='top' data-toggle='tooltip' data-original-title='Unggah Data Dukung Lainnya'><i class='fa fa-upload'></i></a>";
                                                } else {
                                                    $btn_misc = "<a href='".base_url()."uploads/ankabut_uke/misc_kegiatan/".$usulan->file_misc."' type='button' class='btn btn-info btn-xs tooltips btn-unduh' data-placement='top' data-toggle='tooltip' data-original-title='Unduh Data Dukung Lainnya'><i class='fa fa-download'></i></a>"; 
                                                    $btn_misc .= "<a href='".base_url()."ankabut_uke/hapus_file/3/".$usulan->id."' type='button' class='btn btn-danger btn-xs tooltips btn-hapus-file' data-placement='top' data-toggle='tooltip' data-original-title='Hapus Data Dukung Lainnya'><i class='fa fa-trash'></i></a>";
                                                }
                                            ?>
                                            <tr>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-xs tooltips btn-hapus" data-placement="top" data-toggle="tooltip" data-original-title="Hapus Data" data-id="<?=$usulan->id?>"><i class="fa fa-trash"></i></button>
                                                </td>
                                                <td><?=$usulan->kdsatker?></td>
                                                <td><?=$usulan->nama_satker?></td>
                                                <td><?=$usulan->tahun?></td>
                                                <td><?=number_format($usulan->ak_pagu, 0, ',', '.')?></td>
                                                <td><?=number_format($usulan->ak_bp, 0, ',', '.')?></td>
                                                <td><?=number_format($usulan->ak_bo, 0, ',', '.')?></td>
                                                <td><?=number_format($usulan->ak_bbo, 0, ',', '.')?></td>
                                                <td><?=number_format($usulan->ak_bm, 0, ',', '.')?></td>
                                                <td><?=number_format($usulan->uke_pagu, 0, ',', '.')?></td>
                                                <td><?=number_format($usulan->uke_bp, 0, ',', '.')?></td>
                                                <td><?=number_format($usulan->uke_bo, 0, ',', '.')?></td>
                                                <td><?=number_format($usulan->uke_bbo, 0, ',', '.')?></td>
                                                <td><?=number_format($usulan->uke_bm, 0, ',', '.')?></td>
                                                <td><?=$btn_kak_tor?></td>
                                                <td><?=$btn_rab?></td>
                                                <td><?=$btn_misc?></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <ul class="pagination pull-right">
                                        <?=$this->pagination->create_links();?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
    <!--main content end-->

    <?=form_open('ankabut_uke/hapus_usulan/', 'autocomplete="off" class="form-horizontal" role="form"'); ?>
    <div class="modal fade" id="modal-hapus">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Hapus Usulan Sarpras</h4>
                </div>
                <div class="modal-body">
                    Setuju menghapus data ini ?
                    <input type="hidden" class="form-control" name="id" id="id-hapus">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Ya, Saya setuju untuk menghapus data ini <i class="fa fa-ban"></i></button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <?=form_close(); ?>

<?php
include(__DIR__ . "/../footer.php");
?>

<script>
    $(function () {

        var tableusulan = $('#table-usulan').DataTable({
          "scrollX": true,
          "lengthMenu": [[50, 100, -1], [50, 100, "All"]],
          "paging": true,
          "ordering": false,
          "searching": false,
          "scrollY": "500px",
          "scrollCollapse": true
        });

        
        <?php if ($keyword != "ALL") { ?>
        $('#keyword').val("<?=$keyword?>");
        <?php } ?>

        $('#btn-cari').click(function(ev){
            ev.preventDefault();
            if ($('#keyword').val() != "") {
                var keyword = $('#keyword').val();
            } else {
                var keyword = "all";
            }
            var url = "<?=base_url()?>ankabut_uke/usulan/" + keyword + "/";
            window.open(url,'_self');
        });

        $('#table-usulan').on('click', '.btn-hapus', function(ev){
            ev.preventDefault();
            $('#id-hapus').val($(this).data('id'));
            $('#modal-hapus').modal();
        });

    });
</script>

</body>
</html>
