<?php
include(__DIR__ . "/../head.php");
include(__DIR__ . "/../header.php");
include(__DIR__ . "/../sidebar.php");
?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            
            <?php if (isset($info)) { ?>
            <div class="alert <?=$info_type?> fade in">
                <button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>
                <?=$info_pesan?>
            </div>
            <?php } ?>

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Unggah File Kegiatan
                        </header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="Masukkan nama satuan kerja atau nama kegiatan yang akan dicari.." name="keyword" id="keyword">
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary btn-sm" id="btn-cari"><i class="fa fa-search"></i> Cari Data</button>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="table-file" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Kode Satker</th>
                                                <th>Nama Satker</th>
                                                <th>Nama Kegiatan</th>
                                                <th>KAK / TOR</th>
                                                <th>RAB</th>
                                                <th>Data Dukung Lainnya</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            $no = $this->uri->segment(4, 0);
                                            foreach($files as $file) {
                                                $no++;

                                                if ($file->file_kak_tor == '-') {
                                                    $btn_kak_tor = "<a href='".base_url()."kegiatan/unggah_file_dialog/1/".$file->id."' type='button' class='btn btn-info btn-xs tooltips btn-unggah' data-placement='top' data-toggle='tooltip' data-original-title='Unggah KAK / TOR'><i class='fa fa-upload'></i></a>";
                                                } else {
                                                    $btn_kak_tor = "<a href='".base_url()."uploads/kak_tor_kegiatan/".$file->file_kak_tor."' type='button' class='btn btn-info btn-xs tooltips btn-unduh' data-placement='top' data-toggle='tooltip' data-original-title='Unduh KAK / TOR'><i class='fa fa-download'></i></a>";
                                                    $btn_kak_tor .= " <a href='".base_url()."kegiatan/hapus_file_kegiatan/1/".$file->id."' type='button' class='btn btn-danger btn-xs tooltips btn-hapus' data-placement='top' data-toggle='tooltip' data-original-title='Hapus KAK / TOR'><i class='fa fa-trash'></i></a>";
                                                }

                                                if ($file->file_rab == '-') {
                                                    $btn_rab = "<a href='".base_url()."kegiatan/unggah_file_dialog/2/".$file->id."' type='button' class='btn btn-info btn-xs tooltips btn-unggah' data-placement='top' data-toggle='tooltip' data-original-title='Unggah RAB'><i class='fa fa-upload'></i></a>";
                                                } else {
                                                    $btn_rab = "<a href='".base_url()."uploads/rab_kegiatan/".$file->file_rab."' type='button' class='btn btn-info btn-xs tooltips btn-unduh' data-placement='top' data-toggle='tooltip' data-original-title='Unduh RAB'><i class='fa fa-download'></i></a>";
                                                    $btn_rab .= " <a href='".base_url()."kegiatan/hapus_file_kegiatan/2/".$file->id."' type='button' class='btn btn-danger btn-xs tooltips btn-hapus' data-placement='top' data-toggle='tooltip' data-original-title='Hapus RAB'><i class='fa fa-trash'></i></a>";
                                                }

                                                if ($file->file_misc == '-') {
                                                    $btn_misc = "<a href='".base_url()."kegiatan/unggah_file_dialog/3/".$file->id."' type='button' class='btn btn-info btn-xs tooltips btn-unggah' data-placement='top' data-toggle='tooltip' data-original-title='Unggah Data Dukung Lainnya'><i class='fa fa-upload'></i></a>";
                                                } else {
                                                    $btn_misc = "<a href='".base_url()."uploads/misc_kegiatan/".$file->file_misc."' type='button' class='btn btn-info btn-xs tooltips btn-unduh' data-placement='top' data-toggle='tooltip' data-original-title='Unduh Data Dukung Lainnya'><i class='fa fa-download'></i></a>"; 
                                                    $btn_misc .= "<a href='".base_url()."kegiatan/hapus_file_kegiatan/3/".$file->id."' type='button' class='btn btn-danger btn-xs tooltips btn-hapus' data-placement='top' data-toggle='tooltip' data-original-title='Hapus Data Dukung Lainnya'><i class='fa fa-trash'></i></a>";
                                                }
                                                
                                            ?>
                                            <tr>
                                                <td><?=$no?></td>
                                                <td><?=$file->kdsatker?></td>
                                                <td><?=$file->nama_satker?></td>
                                                <td><?=$file->nama_kegiatan?></td>
                                                <td><?=$btn_kak_tor?></td>
                                                <td><?=$btn_rab?></td>
                                                <td><?=$btn_misc?></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <ul class="pagination pull-right">
                                        <?=$this->pagination->create_links();?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
    <!--main content end-->

<?php
include(__DIR__ . "/../footer.php");
?>

<script>
    $(function () {
        
        <?php if ($keyword != "ALL") { ?>
        $('#keyword').val("<?=$keyword?>");
        <?php } ?>

        $('#btn-cari').click(function(ev){
            ev.preventDefault();
            if ($('#keyword').val() != "") {
                var keyword = $('#keyword').val();
            } else {
                var keyword = "all";
            }
            var url = "<?=base_url()?>kegiatan/unggah_file/" + keyword + "/";
            window.open(url,'_self');
        });

    });
</script>

</body>
</html>
