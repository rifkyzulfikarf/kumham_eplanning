<?php
include(__DIR__ . "/../head.php");
include(__DIR__ . "/../header.php");
include(__DIR__ . "/../sidebar.php");
?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            
            <?php if (isset($info)) { ?>
            <div class="alert <?=$info_type?> fade in">
                <button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>
                <?=$info_pesan?>
            </div>
            <?php } ?>

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Master Data Satuan Kerja
                            <button type="button" class="btn btn-info btn-sm pull-right"><i class="fa fa-plus"></i> Tambah Data</button>
                        </header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="Masukkan nama satuan kerja yang akan dicari.." name="keyword" id="keyword">
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary btn-sm" id="btn-cari"><i class="fa fa-search"></i> Cari Data</button>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="table-satker" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Kode</th>
                                                <th>Nama Satker</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            $no = $this->uri->segment(4, 0);
                                            foreach($satkers as $satker) {
                                                $no++;
                                            ?>
                                            <tr>
                                                <td><?=$no?></td>
                                                <td><?=$satker->kode?></td>
                                                <td><?=$satker->nama?></td>
                                                <td>
                                                    <button type="button" class="btn btn-info btn-xs tooltips btn-ubah" data-placement="top" data-toggle="tooltip" data-original-title="Ubah Data" data-id="<?=$satker->kode?>"><i class="fa fa-pencil"></i></button> 
                                                    <button type="button" class="btn btn-danger btn-xs tooltips btn-hapus" data-placement="top" data-toggle="tooltip" data-original-title="Hapus Data" data-id="<?=$satker->kode?>"><i class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <ul class="pagination pull-right">
                                        <?=$this->pagination->create_links();?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
    <!--main content end-->

    <?=form_open('satker/hapus/', 'autocomplete="off" class="form-horizontal" role="form"'); ?>
    <div class="modal fade" id="modal-hapus">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Hapus Sifat Surat</h4>
                </div>
                <div class="modal-body">
                    Setuju menghapus data ini ?
                    <input type="hidden" class="form-control" name="id" id="id-hapus">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Ya, Saya setuju untuk menghapus data ini <i class="fa fa-ban"></i></button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <?=form_close(); ?>

<?php
include(__DIR__ . "/../footer.php");
?>

<script>
    $(function () {
        
        <?php if ($keyword != "ALL") { ?>
        $('#keyword').val("<?=$keyword?>");
        <?php } ?>

        $('#btn-cari').click(function(ev){
            ev.preventDefault();
            if ($('#keyword').val() != "") {
                var keyword = $('#keyword').val();
            } else {
                var keyword = "all";
            }
            var url = "<?=base_url()?>satker/master/" + keyword + "/";
            window.open(url,'_self');
        });

        $('#table-satker').on('click', '.btn-ubah', function(ev){
            ev.preventDefault();
            var satker_id = $(this).data('id');
            var url = "<?=base_url()?>satker/ubah/" + satker_id + "/";
            window.open(url,'_self');
        });

        $('#table-satker').on('click', '.btn-hapus', function(ev){
            ev.preventDefault();
            $('#id-hapus').val($(this).data('id'));
            $('#modal-hapus').modal();
        });

    });
</script>

</body>
</html>
