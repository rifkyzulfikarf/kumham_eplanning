<?php
include(__DIR__ . "/../head.php");
include(__DIR__ . "/../header.php");
include(__DIR__ . "/../sidebar.php");
?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            
            <?php if (isset($info)) { ?>
            <div class="alert <?=$info_type?> fade in">
                <button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>
                <?=$info_pesan?>
            </div>
            <?php } ?>

            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <section class="panel">
                        <header class="panel-heading">Profil Saya</header>
                        <div class="panel-body">
                            <?=form_open('user/profil_saya/', 'autocomplete="off" class="form-horizontal" role="form"'); ?>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Nama</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" placeholder="Masukkan nama anda..." name="nama" value="<?=$user->realname?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Username</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" placeholder="Masukkan username login anda..." name="username" value="<?=$user->username?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Password</label>
                                    <div class="col-lg-10">
                                        <input type="password" class="form-control" placeholder="Masukkan password login anda..." name="password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Password Lagi</label>
                                    <div class="col-lg-10">
                                        <input type="password" class="form-control" placeholder="Masukkan lagi password login anda..." name="password2">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 col-sm-2 control-label">Pilih avatar</label>
                                    <div class="col-lg-2">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="avatar" value="avatar-mini.jpg" checked="">
                                                <img src="<?=base_url()?>assets/backend/img/avatar-mini.jpg">
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="avatar" value="avatar-mini2.jpg">
                                                <img src="<?=base_url()?>assets/backend/img/avatar-mini2.jpg">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="avatar" value="avatar-mini3.jpg">
                                                <img src="<?=base_url()?>assets/backend/img/avatar-mini3.jpg">
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="avatar" value="avatar-mini4.jpg">
                                                <img src="<?=base_url()?>assets/backend/img/avatar-mini4.jpg">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button type="submit" class="btn btn-primary" name="btnSubmit">Simpan Perubahan <i class="fa fa-send"></i></button>
                                    </div>
                                </div>
                            <?=form_close(); ?>
                        </div>
                    </section>
                </div>
                <div class="col-lg-2"></div>
            </div>
        </section>
    </section>
    <!--main content end-->

<?php
include(__DIR__ . "/../footer.php");
?>

</body>
</html>
