<?php
include(__DIR__ . "/../head.php");
include(__DIR__ . "/../header.php");
include(__DIR__ . "/../sidebar.php");
?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            
            <?php if (isset($info)) { ?>
            <div class="alert <?=$info_type?> fade in">
                <button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>
                <?=$info_pesan?>
            </div>
            <?php } ?>

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Data Trilateral Meeting
                            <a href="<?=base_url()?>tm/tambah/" type="button" class="btn btn-info btn-sm pull-right"><i class="fa fa-plus"></i> Tambah Data</a>
                        </header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="Masukkan nama file yang akan dicari.." name="keyword" id="keyword">
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary btn-sm" id="btn-cari"><i class="fa fa-search"></i> Cari Data</button>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="table-tm" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Judul File</th>
                                                <th>File</th>
                                                <th>Highlight</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            $no = $this->uri->segment(4, 0);
                                            foreach($tms as $tm) {
                                                $no++;

                                                if ($tm->is_highlight == "1") {
                                                    $button_highlight = "<a href='".base_url()."tm/set_unhighlight/".$tm->id."' type='button' class='btn btn-warning btn-xs tooltips' data-placement='top' data-toggle='tooltip' data-original-title='Matikan Highlight' data-id='".$tm->id."'><i class='fa fa-magic'></i></a>";
                                                } else {
                                                    $button_highlight = "<a href='".base_url()."tm/set_highlight/".$tm->id."' type='button' class='btn btn-primary btn-xs tooltips' data-placement='top' data-toggle='tooltip' data-original-title='Jadikan Highlight' data-id='".$tm->id."'><i class='fa fa-magic'></i></a>";
                                                }
                                            ?>
                                            <tr>
                                                <td><?=$no?></td>
                                                <td><?=$tm->judul_file?></td>
                                                <td><?=$tm->path_file?></td>
                                                <td><?php echo ($tm->is_highlight == '1')?"Ya":"Tidak"; ?></td>
                                                <td>
                                                    <?=$button_highlight?> 
                                                    <a href="<?=base_url()?>uploads/tm/<?=$tm->path_file?>" type="button" class="btn btn-primary btn-xs tooltips" data-placement="top" data-toggle="tooltip" data-original-title="Unduh File" target="_blank"><i class="fa fa-download"></i></a> 
                                                    <button type="button" class="btn btn-danger btn-xs tooltips btn-hapus" data-placement="top" data-toggle="tooltip" data-original-title="Hapus Data" data-id="<?=$tm->id?>"><i class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <ul class="pagination pull-right">
                                        <?=$this->pagination->create_links();?>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <p><i class="fa fa-info-circle"></i> Item ter-highlight akan tampil sebagai preview PDF di halaman publik. Sementara yang tidak ter-highlight hanya akan muncul sebagai link unduh.</p>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
    <!--main content end-->

    <?=form_open('tm/hapus/', 'autocomplete="off" class="form-horizontal" role="form"'); ?>
    <div class="modal fade" id="modal-hapus">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Hapus Sifat Surat</h4>
                </div>
                <div class="modal-body">
                    Setuju menghapus data ini ?
                    <input type="hidden" class="form-control" name="id" id="id-hapus">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Ya, Saya setuju untuk menghapus data ini <i class="fa fa-ban"></i></button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <?=form_close(); ?>

<?php
include(__DIR__ . "/../footer.php");
?>

<script>
    $(function () {
        
        <?php if ($keyword != "ALL") { ?>
        $('#keyword').val("<?=$keyword?>");
        <?php } ?>

        $('#btn-cari').click(function(ev){
            ev.preventDefault();
            if ($('#keyword').val() != "") {
                var keyword = $('#keyword').val();
            } else {
                var keyword = "all";
            }
            var url = "<?=base_url()?>tm/master/" + keyword + "/";
            window.open(url,'_self');
        });

        $('#table-tm').on('click', '.btn-hapus', function(ev){
            ev.preventDefault();
            $('#id-hapus').val($(this).data('id'));
            $('#modal-hapus').modal();
        });

    });
</script>

</body>
</html>
