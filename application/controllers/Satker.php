<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Satker extends CI_Controller {

	public $model_satker = NULL;

	public function __construct() {
		parent::__construct();

		$this->load->model('Satker_Model');
		$this->model_satker = $this->Satker_Model;
	}

	public function master() {
		if ($this->session->userdata('app-id') !== null) {
			$this->load->library('pagination');
			
			$keyword = strtoupper(urldecode($this->uri->segment(3, "all")));

			if ($keyword == "ALL") {
				$search = "%";
			} else {
				$search = $keyword;
			}
			$jumlah_data = $this->model_satker->jumlah_data($search);

			$config['base_url'] = base_url().'satker/master/'.$keyword.'/';
			$config['uri_segment'] = 4;
			$config['total_rows'] = $jumlah_data->jumlah;
			$config['per_page'] = 20;
			$config['num_links'] = 2;
			$config['first_link'] = '«';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = '»';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['prev_link'] = 'Prev';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li><a href='#'><b>";
			$config['cur_tag_close'] = '</b></a></li>';
			$config['next_link'] = 'Next';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$this->pagination->initialize($config);		

			$from = $this->uri->segment(4, 0);
			$data['satkers'] = $this->model_satker->get_data($search, $config['per_page'], $from);
			$data['keyword'] = urldecode($keyword);

			if ($this->session->flashdata('info') !== null) {
				$data['info'] = $this->session->flashdata('info');
				$data['info_type'] = $this->session->flashdata('info_type');
				$data['info_pesan'] = $this->session->flashdata('info_pesan');
			}

			$this->load->view('satker/mst_satker',$data);
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}
	
}
