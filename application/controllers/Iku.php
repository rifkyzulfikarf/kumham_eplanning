<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Iku extends CI_Controller {

	public $model_iku = NULL;

	public function __construct() {
		parent::__construct();

		$this->load->model('Iku_Model');
		$this->model_iku = $this->Iku_Model;
	}

	public function master() {
		if ($this->session->userdata('app-id') !== null) {
			$this->load->library('pagination');
			
			$keyword = strtoupper(urldecode($this->uri->segment(3, "all")));

			if ($keyword == "ALL") {
				$search = "%";
			} else {
				$search = $keyword;
			}
			$jumlah_data = $this->model_iku->jumlah_data($search);

			$config['base_url'] = base_url().'iku/master/'.$keyword.'/';
			$config['uri_segment'] = 4;
			$config['total_rows'] = $jumlah_data->jumlah;
			$config['per_page'] = 20;
			$config['num_links'] = 2;
			$config['first_link'] = '<<';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = '>>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['prev_link'] = '<';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li><a href='#'><b>";
			$config['cur_tag_close'] = '</b></a></li>';
			$config['next_link'] = '>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$this->pagination->initialize($config);		

			$from = $this->uri->segment(4, 0);
			$data['ikus'] = $this->model_iku->get_data($search, $config['per_page'], $from);
			$data['keyword'] = urldecode($keyword);

			if ($this->session->flashdata('info') !== null) {
				$data['info'] = $this->session->flashdata('info');
				$data['info_type'] = $this->session->flashdata('info_type');
				$data['info_pesan'] = $this->session->flashdata('info_pesan');
			}

			$this->load->view('iku/mst_iku',$data);
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function tambah(){
		if ($this->session->userdata('app-id') !== null) {
			if (isset($_POST['btnSubmit'])) {
				$this->load->library('upload');

				$this->form_validation->set_rules('judul', 'Judul', 'trim|required');
				if (empty($_FILES['file']['name'])) {
					$this->form_validation->set_rules('file', 'Document', 'required');
				}

				if ($this->form_validation->run() == FALSE) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda : ".validation_errors());
					header("Location: ".base_url()."iku/tambah/");
					exit;
				} else {


					$fileName = time().$_FILES['file']['name'];
         
			        $config['upload_path'] = './uploads/iku/';
			        $config['file_name'] = $fileName;
			        $config['allowed_types'] = 'pdf';
			        $config['max_size'] = 60000;

			        $this->upload->initialize($config);

			        if(!$this->upload->do_upload('file')) {
		        	
			        	$this->session->set_flashdata('info', true);
						$this->session->set_flashdata('info_type', "alert-danger");
						$this->session->set_flashdata('info_pesan', "Upload gagal. ".$this->upload->display_errors());
						header("Location: ".base_url()."iku/tambah/");
						exit;

			        } else {
			        	$simpan = $this->model_iku->tambah(
							$_POST['judul'],
							$fileName,
							"0",
							"99"
							);

						if ($simpan) {
							$this->session->set_flashdata('info', true);
							$this->session->set_flashdata('info_type', "alert-success");
							$this->session->set_flashdata('info_pesan', "Simpan data berhasil.");
							header("Location: ".base_url()."iku/master/");
							exit;
						} else {
							$this->session->set_flashdata('info', true);
							$this->session->set_flashdata('info_type', "alert-danger");
							$this->session->set_flashdata('info_pesan', "Simpan data gagal.");
							header("Location: ".base_url()."iku/master/");
							exit;
						}
			        }
				}
			} else {
				if ($this->session->flashdata('info') !== null) {
					$this->load->view('iku/tambah_iku', 
						[
							"info" => $this->session->flashdata('info'), 
							"info_type" => $this->session->flashdata('info_type'),
							"info_pesan" => $this->session->flashdata('info_pesan')
						]);
				} else {
					$this->load->view('iku/tambah_iku');
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function hapus(){
		if ($this->session->userdata('app-id') !== null) {
			$this->form_validation->set_rules('id', 'Kode', 'trim|required',
				array('required' => 'Kode harus diisi')
			);

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-danger");
				$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda.".validation_errors());
				header("Location: ".base_url()."iku/master/");
				exit;
			} else {
				$path = $this->model_iku->get_data_by_id($_POST['id']);

				if (file_exists(FCPATH.'uploads/iku/'.$path->path_file)) {   
					unlink(FCPATH.'uploads/iku/'.$path->path_file);
				}

				$simpan = $this->model_iku->hapus(
					$_POST['id']
				);
				if ($simpan) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-success");
					$this->session->set_flashdata('info_pesan', "Hapus data berhasil.");
					header("Location: ".base_url()."iku/master/");
					exit;
				} else {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Hapus data gagal.");
					header("Location: ".base_url()."iku/master/");
					exit;
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function set_highlight() {
		if ($this->session->userdata('app-id') !== null) {
			$id = $this->uri->segment(3, 0);

			if ($this->model_iku->check_highlight_exist()) {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-danger");
				$this->session->set_flashdata('info_pesan', "Set highlight gagal. File yang dapat ter-highlight hanya boleh 1.");
				header("Location: ".base_url()."iku/master/");
				exit;
			} else {
				$set = $this->model_iku->set_highlight($id);
				if ($set) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-success");
					$this->session->set_flashdata('info_pesan', "Set highlight berhasil.");
					header("Location: ".base_url()."iku/master/");
					exit;
				} else {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Set highlight gagal.");
					header("Location: ".base_url()."iku/master/");
					exit;
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function set_unhighlight() {
		if ($this->session->userdata('app-id') !== null) {
			$id = $this->uri->segment(3, 0);

			$set = $this->model_iku->set_unhighlight($id);
			if ($set) {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-success");
				$this->session->set_flashdata('info_pesan', "Nonaktif highlight berhasil.");
				header("Location: ".base_url()."iku/master/");
				exit;
			} else {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-danger");
				$this->session->set_flashdata('info_pesan', "Nonaktif highlight gagal.");
				header("Location: ".base_url()."iku/master/");
				exit;
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function frontend() {
		if ($this->session->flashdata('info') !== null) {
			$data['info'] = $this->session->flashdata('info');
			$data['info_type'] = $this->session->flashdata('info_type');
			$data['info_pesan'] = $this->session->flashdata('info_pesan');
		}

		$data['ikus'] = $this->model_iku->get_data('%', 10, 0);
		$data['highlight'] = $this->model_iku->get_highlighted_row();

		$this->load->view('public/iku',$data);
	}
	
}
