<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Abt extends CI_Controller {

	public $model_abt = NULL;
	public function __construct() {
		parent::__construct();

		$this->load->model('Abt_Model');
		$this->model_abt = $this->Abt_Model;
	}

	public function master() {
		if ($this->session->userdata('app-id') !== null) {
			$this->load->library('pagination');
			
			$keyword = strtoupper(urldecode($this->uri->segment(3, "all")));

			if ($keyword == "ALL") {
				$search = "%";
			} else {
				$search = $keyword;
			}
			$jumlah_data = $this->model_abt->jumlah_data($search);

			$config['base_url'] = base_url().'abt/master/'.$keyword.'/';
			$config['uri_segment'] = 4;
			$config['total_rows'] = $jumlah_data->jumlah;
			$config['per_page'] = 20;
			$config['num_links'] = 2;
			$config['first_link'] = '<<';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = '>>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['prev_link'] = '<';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li><a href='#'><b>";
			$config['cur_tag_close'] = '</b></a></li>';
			$config['next_link'] = '>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$this->pagination->initialize($config);		

			$from = $this->uri->segment(4, 0);
			$data['abts'] = $this->model_abt->get_data($search, $config['per_page'], $from);
			$data['keyword'] = urldecode($keyword);

			if ($this->session->flashdata('info') !== null) {
				$data['info'] = $this->session->flashdata('info');
				$data['info_type'] = $this->session->flashdata('info_type');
				$data['info_pesan'] = $this->session->flashdata('info_pesan');
			}

			$this->load->view('abt/mst_abt',$data);
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function frontend() {
		$data['tahun'] = $this->uri->segment(3, 2018);

		$data['alokasis'] = $this->db->query("SELECT satker.nama_satker, pagu_alokasi.kdsatker, SUM(pagu_alokasi.belanja_pegawai_ops) AS belanja_pegawai_ops, SUM(pagu_alokasi.belanja_barang_ops) AS belanja_barang_ops, SUM(pagu_alokasi.belanja_barang_nonops) AS belanja_barang_nonops, SUM(pagu_alokasi.belanja_modal_nonops) AS belanja_modal_nonops, SUM(pagu_alokasi.jumlah) AS jumlah FROM pagu_alokasi INNER JOIN satker ON pagu_alokasi.kdsatker = satker.kode_satker WHERE pagu_alokasi.tahun = '".$data['tahun']."' GROUP BY pagu_alokasi.kdsatker ORDER BY pagu_alokasi.kdsatker ASC;")->result();

		if ($this->session->flashdata('info') !== null) {
			$this->load->view('public/abt', 
				[
					"info" => $this->session->flashdata('info'), 
					"info_type" => $this->session->flashdata('info_type'),
					"info_pesan" => $this->session->flashdata('info_pesan'),
					$data
				]);
		} else {
			$this->load->view('public/abt', $data);
		}
	}

	public function frontend_kegiatan() {
		$kdsatker = $this->uri->segment(3, 0);
		$data['alokasis'] = $this->db->query("SELECT kegiatan.nama_kegiatan, pagu_alokasi.kdgiat, pagu_alokasi.belanja_pegawai_ops, pagu_alokasi.belanja_barang_ops, pagu_alokasi.belanja_barang_nonops, pagu_alokasi.belanja_modal_nonops, pagu_alokasi.jumlah FROM pagu_alokasi INNER JOIN kegiatan ON pagu_alokasi.kdgiat = kegiatan.kode_kegiatan WHERE pagu_alokasi.tahun = '2018' AND pagu_alokasi.kdsatker = '$kdsatker' ORDER BY pagu_alokasi.kdgiat ASC;")->result();

		if ($this->session->flashdata('info') !== null) {
			$this->load->view('public/abt_kegiatan', 
				[
					"info" => $this->session->flashdata('info'), 
					"info_type" => $this->session->flashdata('info_type'),
					"info_pesan" => $this->session->flashdata('info_pesan'),
					$data
				]);
		} else {
			$this->load->view('public/abt_kegiatan', $data);
		}
	}
	
}
