<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sarpras extends CI_Controller {

	public $model_sarpras = NULL;

	public function __construct() {
		parent::__construct();

		$this->load->model('Sarpras_Model');
		$this->model_sarpras = $this->Sarpras_Model;
	}

	public function usulan() {
		if ($this->session->userdata('app-id') !== null) {
			$this->load->library('pagination');
			
			$keyword = strtoupper(urldecode($this->uri->segment(3, "all")));

			if ($keyword == "ALL") {
				$search = "%";
			} else {
				$search = $keyword;
			}
			$jumlah_data = $this->model_sarpras->jumlah_data($search);

			$config['base_url'] = base_url().'sarpras/usulan/'.$keyword.'/';
			$config['uri_segment'] = 4;
			$config['total_rows'] = $jumlah_data->jumlah;
			$config['per_page'] = 20;
			$config['num_links'] = 2;
			$config['first_link'] = '<<';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = '>>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['prev_link'] = '<';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li><a href='#'><b>";
			$config['cur_tag_close'] = '</b></a></li>';
			$config['next_link'] = '>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$this->pagination->initialize($config);		

			$from = $this->uri->segment(4, 0);
			$data['usulans'] = $this->model_sarpras->get_data($search, $config['per_page'], $from);
			$data['keyword'] = urldecode($keyword);

			if ($this->session->flashdata('info') !== null) {
				$data['info'] = $this->session->flashdata('info');
				$data['info_type'] = $this->session->flashdata('info_type');
				$data['info_pesan'] = $this->session->flashdata('info_pesan');
			}

			$this->load->view('sarpras/usulan',$data);
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function tambah_usulan(){
		if ($this->session->userdata('app-id') !== null) {
			if (isset($_POST['btnSubmit'])) {

				$this->form_validation->set_rules('satuan', 'Satuan', 'trim|required');
				$this->form_validation->set_rules('harga', 'Harga', 'trim|required');
				$this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
				$this->form_validation->set_rules('total', 'Total', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda : ".validation_errors());
					header("Location: ".base_url()."sarpras/tambah_usulan/");
					exit;
				} else {

					$ppdk = (isset($_POST['ppdk'])) ? $_POST['ppdk']:"0";
					$pfk = (isset($_POST['pfk'])) ? $_POST['pfk']:"0";
					$kb = (isset($_POST['kb'])) ? $_POST['kb']:"0";
					$pb = (isset($_POST['pb'])) ? $_POST['pb']:"0";
					$rehab = (isset($_POST['rehab'])) ? $_POST['rehab']:"0";
					$renov = (isset($_POST['renov'])) ? $_POST['renov']:"0";
					$pl = (isset($_POST['pl'])) ? $_POST['pl']:"0";

		        	$simpan = $this->model_sarpras->tambah_usulan(
						$_POST['satker'],
						$_POST['kegiatan'],
						$_POST['tahun'],
						$ppdk,
						$pfk,
						$kb,
						$pb,
						$rehab,
						$renov,
						$pl,
						$_POST['satuan'],
						$_POST['harga'],
						$_POST['jumlah'],
						$_POST['total']
					);

					if ($simpan) {
						$this->session->set_flashdata('info', true);
						$this->session->set_flashdata('info_type', "alert-success");
						$this->session->set_flashdata('info_pesan', "Simpan data berhasil.");
						header("Location: ".base_url()."sarpras/usulan/");
						exit;
					} else {
						$this->session->set_flashdata('info', true);
						$this->session->set_flashdata('info_type', "alert-danger");
						$this->session->set_flashdata('info_pesan', "Simpan data gagal.");
						header("Location: ".base_url()."sarpras/usulan/");
						exit;
					}
				}
			} else {
				if ($this->session->flashdata('info') !== null) {
					$this->load->view('sarpras/tambah_usulan', 
						[
							"info" => $this->session->flashdata('info'), 
							"info_type" => $this->session->flashdata('info_type'),
							"info_pesan" => $this->session->flashdata('info_pesan')
						]);
				} else {
					$this->load->view('sarpras/tambah_usulan');
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function hapus_usulan(){
		if ($this->session->userdata('app-id') !== null) {
			$this->form_validation->set_rules('id', 'Kode', 'trim|required',
				array('required' => 'Kode harus diisi')
			);

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-danger");
				$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda.".validation_errors());
				header("Location: ".base_url()."sarpras/usulan/");
				exit;
			} else {
				$simpan = $this->model_sarpras->hapus(
					$_POST['id']
				);
				if ($simpan) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-success");
					$this->session->set_flashdata('info_pesan', "Hapus data berhasil.");
					header("Location: ".base_url()."sarpras/usulan/");
					exit;
				} else {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Hapus data gagal.");
					header("Location: ".base_url()."sarpras/usulan/");
					exit;
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function unggah_file_dialog() {
		if ($this->session->userdata('app-id') !== null) {
			if (isset($_POST['btnSubmit'])) {
				
				$this->load->library('upload');

				$this->form_validation->set_rules('jenis', 'Jenis', 'trim|required');
				$this->form_validation->set_rules('id', 'ID', 'trim|required');
				if (empty($_FILES['file']['name'])) {
					$this->form_validation->set_rules('file', 'Document', 'required');
				}

				if ($this->form_validation->run() == FALSE) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda : ".validation_errors());
					header("Location: ".base_url()."sarpras/unggah_file_dialog/".$jenis."/".$id);
					exit;
				} else {

					$jenis = base64_decode($_POST['jenis']);
					$id = base64_decode($_POST['id']);

					$fileName = time().$_FILES['file']['name'];

					if ($jenis == '1') {
						$config['upload_path'] = './uploads/sarpras/kak_tor_kegiatan/';
					} elseif ($jenis == '2') {
						$config['upload_path'] = './uploads/sarpras/rab_kegiatan/';
					} elseif ($jenis == '3') {
						$config['upload_path'] = './uploads/sarpras/misc_kegiatan/';
					}
         
			        
			        $config['file_name'] = $fileName;
			        $config['allowed_types'] = 'pdf';
			        $config['max_size'] = 60000;

			        $this->upload->initialize($config);

			        if(!$this->upload->do_upload('file')) {
		        	
			        	$this->session->set_flashdata('info', true);
						$this->session->set_flashdata('info_type', "alert-danger");
						$this->session->set_flashdata('info_pesan', "Upload gagal. ".$this->upload->display_errors());
						header("Location: ".base_url()."sarpras/unggah_file_dialog/".$jenis."/".$id);
						exit;

			        } else {
			        	$simpan = $this->model_sarpras->unggah_file_sarpras(
							$id,
							$jenis,
							$fileName
							);

						if ($simpan) {
							$this->session->set_flashdata('info', true);
							$this->session->set_flashdata('info_type', "alert-success");
							$this->session->set_flashdata('info_pesan', "Simpan data berhasil.");
							header("Location: ".base_url()."sarpras/usulan/");
							exit;
						} else {
							$this->session->set_flashdata('info', true);
							$this->session->set_flashdata('info_type', "alert-danger");
							$this->session->set_flashdata('info_pesan', "Simpan data gagal.");
							header("Location: ".base_url()."sarpras/unggah_file_dialog/".$jenis."/".$id);
							exit;
						}
			        }
				}
			} else {
				$jenis = base64_encode($this->uri->segment(3, 0));
				$id = base64_encode($this->uri->segment(4, 0));
				if ($this->session->flashdata('info') !== null) {
					$this->load->view('sarpras/unggah_file_dialog', 
						[
							"info" => $this->session->flashdata('info'), 
							"info_type" => $this->session->flashdata('info_type'),
							"info_pesan" => $this->session->flashdata('info_pesan'),
							"jenis" => $jenis,
							"id" => $id
						]);
				} else {
					$this->load->view('sarpras/unggah_file_dialog', ["jenis"=>$jenis, "id"=>$id]);
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function hapus_file_sarpras() {
		if ($this->session->userdata('app-id') !== null) {
			$jenis = $this->uri->segment(3, 0);
			$id = $this->uri->segment(4, 0);
			$sarpras = $this->model_sarpras->get_file_sarpras_by_id($id);

			if ($jenis == '1') {
				$path = FCPATH . './uploads/sarpras/kak_tor_kegiatan/' . $sarpras->file_kak_tor;
			} elseif ($jenis == '2') {
				$path = FCPATH . './uploads/sarpras/rab_kegiatan/' . $sarpras->file_rab;
			} elseif ($jenis == '3') {
				$path = FCPATH . './uploads/sarpras/misc_kegiatan/' . $sarpras->file_misc;
			}

			if (file_exists($path)) {   
				unlink($path);
			}

			$simpan = $this->model_sarpras->hapus_file_sarpras(
				$id, $jenis
			);
			if ($simpan) {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-success");
				$this->session->set_flashdata('info_pesan', "Hapus data berhasil.");
				header("Location: ".base_url()."sarpras/usulan/");
				exit;
			} else {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-danger");
				$this->session->set_flashdata('info_pesan', "Hapus data gagal.");
				header("Location: ".base_url()."sarpras/usulan/");
				exit;
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function frontend() {
		$data['tahun'] = $this->uri->segment(3, 2018);

		if ($this->session->flashdata('info') !== null) {
			$this->load->view('public/usulan_sarpras', 
				[
					"info" => $this->session->flashdata('info'), 
					"info_type" => $this->session->flashdata('info_type'),
					"info_pesan" => $this->session->flashdata('info_pesan'),
					$data
				]);
		} else {
			$this->load->view('public/usulan_sarpras', $data);
		}
	}
	
}
