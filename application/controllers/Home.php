<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct();

	}

	public function index() {
		if ($this->session->flashdata('info') !== null) {
			$this->load->view('public/home', 
				[
					"info" => $this->session->flashdata('info'), 
					"info_type" => $this->session->flashdata('info_type'),
					"info_pesan" => $this->session->flashdata('info_pesan')
				]);
		} else {
			$this->load->view('public/home');
		}
	}

	public function pagu() {
		$data['tahun'] = $this->uri->segment(3, 2018);

		$data['indikatifs'] = $this->db->query("SELECT satker.nama_satker, pagu_indikatif.kdsatker, SUM(pagu_indikatif.belanja_pegawai_ops) AS belanja_pegawai_ops, SUM(pagu_indikatif.belanja_barang_ops) AS belanja_barang_ops, SUM(pagu_indikatif.belanja_barang_nonops) AS belanja_barang_nonops, SUM(pagu_indikatif.belanja_modal_nonops) AS belanja_modal_nonops, SUM(pagu_indikatif.jumlah) AS jumlah FROM pagu_indikatif INNER JOIN satker ON pagu_indikatif.kdsatker = satker.kode_satker WHERE pagu_indikatif.tahun = '".$data['tahun']."' GROUP BY pagu_indikatif.kdsatker ORDER BY pagu_indikatif.kdsatker ASC;")->result();

		if ($this->session->flashdata('info') !== null) {
			$this->load->view('public/pagu', 
				[
					"info" => $this->session->flashdata('info'), 
					"info_type" => $this->session->flashdata('info_type'),
					"info_pesan" => $this->session->flashdata('info_pesan'),
					$data
				]);
		} else {
			$this->load->view('public/pagu', $data);
		}
	}

	public function kegiatan() {
		$kdsatker = $this->uri->segment(3, 0);
		$data['indikatifs'] = $this->db->query("SELECT kegiatan.nama_kegiatan, pagu_indikatif.kdgiat, pagu_indikatif.belanja_pegawai_ops, pagu_indikatif.belanja_barang_ops, pagu_indikatif.belanja_barang_nonops, pagu_indikatif.belanja_modal_nonops, pagu_indikatif.jumlah FROM pagu_indikatif INNER JOIN kegiatan ON pagu_indikatif.kdgiat = kegiatan.kode_kegiatan WHERE pagu_indikatif.tahun = '2018' AND pagu_indikatif.kdsatker = '$kdsatker' ORDER BY pagu_indikatif.kdgiat ASC;")->result();

		if ($this->session->flashdata('info') !== null) {
			$this->load->view('public/kegiatan', 
				[
					"info" => $this->session->flashdata('info'), 
					"info_type" => $this->session->flashdata('info_type'),
					"info_pesan" => $this->session->flashdata('info_pesan'),
					$data
				]);
		} else {
			$this->load->view('public/kegiatan', $data);
		}
	}
	
}
