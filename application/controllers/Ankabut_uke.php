<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ankabut_uke extends CI_Controller {

	public $model_ankabut_uke = NULL;

	public function __construct() {
		parent::__construct();

		$this->load->model('Ankabut_Uke_Model');
		$this->model_ankabut_uke = $this->Ankabut_Uke_Model;
	}

	public function usulan() {
		if ($this->session->userdata('app-id') !== null) {
			$this->load->library('pagination');
			
			$keyword = strtoupper(urldecode($this->uri->segment(3, "all")));

			if ($keyword == "ALL") {
				$search = "%";
			} else {
				$search = $keyword;
			}
			$jumlah_data = $this->model_ankabut_uke->jumlah_data($search);

			$config['base_url'] = base_url().'ankabut/usulan/'.$keyword.'/';
			$config['uri_segment'] = 4;
			$config['total_rows'] = $jumlah_data->jumlah;
			$config['per_page'] = 20;
			$config['num_links'] = 2;
			$config['first_link'] = '<<';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = '>>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['prev_link'] = '<';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li><a href='#'><b>";
			$config['cur_tag_close'] = '</b></a></li>';
			$config['next_link'] = '>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$this->pagination->initialize($config);		

			$from = $this->uri->segment(4, 0);
			$data['usulans'] = $this->model_ankabut_uke->get_data($search, $config['per_page'], $from);
			$data['keyword'] = urldecode($keyword);

			if ($this->session->flashdata('info') !== null) {
				$data['info'] = $this->session->flashdata('info');
				$data['info_type'] = $this->session->flashdata('info_type');
				$data['info_pesan'] = $this->session->flashdata('info_pesan');
			}

			$this->load->view('ankabut/usulan',$data);
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function tambah_usulan(){
		if ($this->session->userdata('app-id') !== null) {
			if (isset($_POST['btnSubmit'])) {

				$this->form_validation->set_rules('ak_pagu', 'Pagu Ankabut', 'trim|required');
				$this->form_validation->set_rules('ak_bp', 'BP Ankabut', 'trim|required');
				$this->form_validation->set_rules('ak_bo', 'BO Ankabut', 'trim|required');
				$this->form_validation->set_rules('ak_bbo', 'BBO Ankabut', 'trim|required');
				$this->form_validation->set_rules('ak_bm', 'BM Ankabut', 'trim|required');
				$this->form_validation->set_rules('uke_pagu', 'Pagu Ankabut', 'trim|required');
				$this->form_validation->set_rules('uke_bp', 'BP Ankabut', 'trim|required');
				$this->form_validation->set_rules('uke_bo', 'BO Ankabut', 'trim|required');
				$this->form_validation->set_rules('uke_bbo', 'BBO Ankabut', 'trim|required');
				$this->form_validation->set_rules('uke_bm', 'BM Ankabut', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda : ".validation_errors());
					header("Location: ".base_url()."ankabut_uke/tambah_usulan/");
					exit;
				} else {

		        	$simpan = $this->model_ankabut_uke->tambah_usulan(
						$_POST['satker'],
						$_POST['tahun'],
						$_POST['ak_pagu'],
						$_POST['ak_bp'],
						$_POST['ak_bo'],
						$_POST['ak_bbo'],
						$_POST['ak_bm'],
						$_POST['uke_pagu'],
						$_POST['uke_bp'],
						$_POST['uke_bo'],
						$_POST['uke_bbo'],
						$_POST['uke_bm']
					);

					if ($simpan) {
						$this->session->set_flashdata('info', true);
						$this->session->set_flashdata('info_type', "alert-success");
						$this->session->set_flashdata('info_pesan', "Simpan data berhasil.");
						header("Location: ".base_url()."ankabut_uke/usulan/");
						exit;
					} else {
						$this->session->set_flashdata('info', true);
						$this->session->set_flashdata('info_type', "alert-danger");
						$this->session->set_flashdata('info_pesan', "Simpan data gagal.". $this->db->last_query());
						header("Location: ".base_url()."ankabut_uke/usulan/");
						exit;
					}
				}
			} else {
				if ($this->session->flashdata('info') !== null) {
					$this->load->view('ankabut/tambah_usulan', 
						[
							"info" => $this->session->flashdata('info'), 
							"info_type" => $this->session->flashdata('info_type'),
							"info_pesan" => $this->session->flashdata('info_pesan')
						]);
				} else {
					$this->load->view('ankabut/tambah_usulan');
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function hapus_usulan(){
		if ($this->session->userdata('app-id') !== null) {
			$this->form_validation->set_rules('id', 'Kode', 'trim|required',
				array('required' => 'Kode harus diisi')
			);

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-danger");
				$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda.".validation_errors());
				header("Location: ".base_url()."ankabut_uke/usulan/");
				exit;
			} else {
				$simpan = $this->model_ankabut_uke->hapus(
					$_POST['id']
				);
				if ($simpan) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-success");
					$this->session->set_flashdata('info_pesan', "Hapus data berhasil.");
					header("Location: ".base_url()."ankabut_uke/usulan/");
					exit;
				} else {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Hapus data gagal.");
					header("Location: ".base_url()."ankabut_uke/usulan/");
					exit;
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function unggah_file_dialog() {
		if ($this->session->userdata('app-id') !== null) {
			if (isset($_POST['btnSubmit'])) {
				
				$this->load->library('upload');

				$this->form_validation->set_rules('jenis', 'Jenis', 'trim|required');
				$this->form_validation->set_rules('id', 'ID', 'trim|required');
				if (empty($_FILES['file']['name'])) {
					$this->form_validation->set_rules('file', 'Document', 'required');
				}

				if ($this->form_validation->run() == FALSE) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda : ".validation_errors());
					header("Location: ".base_url()."ankabut_uke/unggah_file_dialog/".$jenis."/".$id);
					exit;
				} else {

					$jenis = base64_decode($_POST['jenis']);
					$id = base64_decode($_POST['id']);

					$fileName = time().$_FILES['file']['name'];

					if ($jenis == '1') {
						$config['upload_path'] = './uploads/ankabut_uke/kak_tor_kegiatan/';
					} elseif ($jenis == '2') {
						$config['upload_path'] = './uploads/ankabut_uke/rab_kegiatan/';
					} elseif ($jenis == '3') {
						$config['upload_path'] = './uploads/ankabut_uke/misc_kegiatan/';
					}
         
			        
			        $config['file_name'] = $fileName;
			        $config['allowed_types'] = 'pdf';
			        $config['max_size'] = 60000;

			        $this->upload->initialize($config);

			        if(!$this->upload->do_upload('file')) {
		        	
			        	$this->session->set_flashdata('info', true);
						$this->session->set_flashdata('info_type', "alert-danger");
						$this->session->set_flashdata('info_pesan', "Upload gagal. ".$this->upload->display_errors());
						header("Location: ".base_url()."ankabut_uke/unggah_file_dialog/".$jenis."/".$id);
						exit;

			        } else {
			        	$simpan = $this->model_ankabut_uke->unggah_file(
							$id,
							$jenis,
							$fileName
							);

						if ($simpan) {
							$this->session->set_flashdata('info', true);
							$this->session->set_flashdata('info_type', "alert-success");
							$this->session->set_flashdata('info_pesan', "Simpan data berhasil.");
							header("Location: ".base_url()."ankabut_uke/usulan/");
							exit;
						} else {
							$this->session->set_flashdata('info', true);
							$this->session->set_flashdata('info_type', "alert-danger");
							$this->session->set_flashdata('info_pesan', "Simpan data gagal.");
							header("Location: ".base_url()."ankabut_uke/unggah_file_dialog/".$jenis."/".$id);
							exit;
						}
			        }
				}
			} else {
				$jenis = base64_encode($this->uri->segment(3, 0));
				$id = base64_encode($this->uri->segment(4, 0));
				if ($this->session->flashdata('info') !== null) {
					$this->load->view('ankabut/unggah_file_dialog', 
						[
							"info" => $this->session->flashdata('info'), 
							"info_type" => $this->session->flashdata('info_type'),
							"info_pesan" => $this->session->flashdata('info_pesan'),
							"jenis" => $jenis,
							"id" => $id
						]);
				} else {
					$this->load->view('ankabut/unggah_file_dialog', ["jenis"=>$jenis, "id"=>$id]);
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function hapus_file() {
		if ($this->session->userdata('app-id') !== null) {
			$jenis = $this->uri->segment(3, 0);
			$id = $this->uri->segment(4, 0);
			$ankabut = $this->model_ankabut_uke->get_file_by_id($id);

			if ($jenis == '1') {
				$path = FCPATH . './uploads/ankabut_uke/kak_tor_kegiatan/' . $ankabut->file_kak_tor;
			} elseif ($jenis == '2') {
				$path = FCPATH . './uploads/ankabut_uke/rab_kegiatan/' . $ankabut->file_rab;
			} elseif ($jenis == '3') {
				$path = FCPATH . './uploads/ankabut_uke/misc_kegiatan/' . $ankabut->file_misc;
			}

			if (file_exists($path)) {   
				unlink($path);
			}

			$simpan = $this->model_ankabut_uke->hapus_file(
				$id, $jenis
			);
			if ($simpan) {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-success");
				$this->session->set_flashdata('info_pesan', "Hapus data berhasil.");
				header("Location: ".base_url()."ankabut_uke/usulan/");
				exit;
			} else {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-danger");
				$this->session->set_flashdata('info_pesan', "Hapus data gagal.");
				header("Location: ".base_url()."ankabut_uke/usulan/");
				exit;
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function frontend() {
		$data['tahun'] = $this->uri->segment(3, 2018);

		if ($this->session->flashdata('info') !== null) {
			$this->load->view('public/usulan_ankabut_uke', 
				[
					"info" => $this->session->flashdata('info'), 
					"info_type" => $this->session->flashdata('info_type'),
					"info_pesan" => $this->session->flashdata('info_pesan'),
					$data
				]);
		} else {
			$this->load->view('public/usulan_ankabut_uke', $data);
		}
	}
	
}
