<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public $model_user = NULL;

	public function __construct() {
		parent::__construct();

		$this->load->model('User_Model');
		$this->model_user = $this->User_Model;
	}

	public function index() {
		if ($this->session->userdata('app-id') !== null) {
			if ($this->session->flashdata('info') !== null) {
				$this->load->view('dashboard', 
					[
						"info" => $this->session->flashdata('info'), 
						"info_type" => $this->session->flashdata('info_type'),
						"info_pesan" => $this->session->flashdata('info_pesan')
					]);
			} else {
				$this->load->view('dashboard');
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function login() {
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('login', ["pesan"=>"Harap isi kolom username dan password..."]);
		} else {
			if($this->model_user->check_login($_POST['username'], $_POST['password'])) {
				header("Location: ".base_url()."user/");
				exit;
			} else {
				$this->load->view('login', ["pesan"=>"Username dan password anda salah..."]);
			}
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		header("Location: ".base_url()."user/");
		exit;
	}

	public function profil_saya() {
		if ($this->session->userdata('app-id') !== null) {
			if (isset($_POST['btnSubmit'])) {
				$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
				$this->form_validation->set_rules('username', 'Provinsi', 'trim|required');
				$this->form_validation->set_rules('password', 'Password', 'trim|required');
				$this->form_validation->set_rules('password2', 'Password Lagi', 'trim|required|matches[password]');

				if ($this->form_validation->run() == FALSE) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda : ".validation_errors());
					header("Location: ".base_url()."user/profil_saya/");
					exit;
				} else {
					$simpan = $this->model_user->ubah($this->session->userdata('app-id'), $_POST['nama'], $_POST['username'], $_POST['password'], $_POST['avatar']);

					if ($simpan) {
						$this->session->set_flashdata('info', true);
						$this->session->set_flashdata('info_type', "alert-success");
						$this->session->set_flashdata('info_pesan', "Simpan data berhasil.");
						$this->session->set_userdata('app-avatar', $_POST['avatar']);
						header("Location: ".base_url()."user/profil_saya/");
						exit;
					} else {
						$this->session->set_flashdata('info', true);
						$this->session->set_flashdata('info_type', "alert-danger");
						$this->session->set_flashdata('info_pesan', "Simpan data gagal.");
						header("Location: ".base_url()."user/profil_saya/");
						exit;
					}
				}
			} else {
				$user = $this->model_user->get_data_by_id($this->session->userdata('app-id'));
				if ($this->session->flashdata('info') !== null) {
					$this->load->view('user/profil_saya', 
						[
							"info" => $this->session->flashdata('info'), 
							"info_type" => $this->session->flashdata('info_type'),
							"info_pesan" => $this->session->flashdata('info_pesan'),
							"user" => $user
						]);
				} else {
					$this->load->view('user/profil_saya', ["user" => $user]);
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function master() {
		if ($this->session->userdata('app-id') !== null) {
			$this->load->library('pagination');
			
			$keyword = strtoupper(urldecode($this->uri->segment(3, "all")));

			if ($keyword == "ALL") {
				$search = "%";
			} else {
				$search = $keyword;
			}
			$jumlah_data = $this->model_user->jumlah_data($search);

			$config['base_url'] = base_url().'user/master/'.$keyword.'/';
			$config['uri_segment'] = 4;
			$config['total_rows'] = $jumlah_data->jumlah;
			$config['per_page'] = 20;
			$config['num_links'] = 2;
			$config['first_link'] = '<<';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = '>>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['prev_link'] = '<';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li><a href='#'><b>";
			$config['cur_tag_close'] = '</b></a></li>';
			$config['next_link'] = '>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$this->pagination->initialize($config);		

			$from = $this->uri->segment(4, 0);
			$data['users'] = $this->model_user->get_data($search, $config['per_page'], $from);
			$data['keyword'] = urldecode($keyword);

			if ($this->session->flashdata('info') !== null) {
				$data['info'] = $this->session->flashdata('info');
				$data['info_type'] = $this->session->flashdata('info_type');
				$data['info_pesan'] = $this->session->flashdata('info_pesan');
			}

			$this->load->view('user/mst_user',$data);
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function tambah(){
		if ($this->session->userdata('app-id') !== null) {
			if (isset($_POST['btnSubmit'])) {
				$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
				$this->form_validation->set_rules('kota', 'Provinsi', 'trim|required');
				$this->form_validation->set_rules('username', 'Username', 'trim|required');
				$this->form_validation->set_rules('password', 'Password', 'trim|required');
				$this->form_validation->set_rules('password2', 'Password Lagi', 'trim|required|matches[password]');
				$this->form_validation->set_rules('admin', 'Administrator', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda : ".validation_errors());
					header("Location: ".base_url()."user/tambah/");
					exit;
				} else {
					$simpan = $this->model_user->tambah($_POST['nama'], $_POST['username'], $_POST['password'], $_POST['kota'], $_POST['admin']);

					if ($simpan) {
						$this->session->set_flashdata('info', true);
						$this->session->set_flashdata('info_type', "alert-success");
						$this->session->set_flashdata('info_pesan', "Simpan data berhasil.");
						header("Location: ".base_url()."user/master/");
						exit;
					} else {
						$this->session->set_flashdata('info', true);
						$this->session->set_flashdata('info_type', "alert-danger");
						$this->session->set_flashdata('info_pesan', "Simpan data gagal.");
						header("Location: ".base_url()."user/tambah/");
						exit;
					}
				}
			} else {
				if ($this->session->flashdata('info') !== null) {
					$this->load->view('user/tambah_user', 
						[
							"info" => $this->session->flashdata('info'), 
							"info_type" => $this->session->flashdata('info_type'),
							"info_pesan" => $this->session->flashdata('info_pesan')
						]);
				} else {
					$this->load->view('user/tambah_user');
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function ubah(){
		if ($this->session->userdata('app-id') !== null) {
			if (isset($_POST['btnSubmit'])) {
				$this->form_validation->set_rules('user_id', 'User ID', 'trim|required');
				$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
				$this->form_validation->set_rules('kota', 'Kab. / Kota', 'trim|required');
				$this->form_validation->set_rules('username', 'NIK', 'trim|required');
				$this->form_validation->set_rules('password', 'Password', 'trim|required');
				$this->form_validation->set_rules('password2', 'Password Lagi', 'trim|required|matches[password]');
				$this->form_validation->set_rules('admin', 'Administrator', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda.".validation_errors());
					header("Location: ".base_url()."user/ubah/".$_POST['user_id']);
					exit;
				} else {
					$simpan = $this->model_user->ubah($_POST['user_id'], $_POST['nama'], $_POST['username'], $_POST['password'], $_POST['kota'], $_POST['admin']);

					if ($simpan) {
						$this->session->set_flashdata('info', true);
						$this->session->set_flashdata('info_type', "alert-success");
						$this->session->set_flashdata('info_pesan', "Simpan data berhasil.");
						header("Location: ".base_url()."user/master/");
						exit;
					} else {
						$this->session->set_flashdata('info', true);
						$this->session->set_flashdata('info_type', "alert-danger");
						$this->session->set_flashdata('info_pesan', "Simpan data gagal.");
						header("Location: ".base_url()."user/ubah/".$_POST['user_id']);
						exit;
					}
				}
			} else {
				$user_id = $this->uri->segment(3, 0);
				if ($this->session->flashdata('info') !== null) {
					$this->load->view('user/ubah_user', 
						[
							"info" => $this->session->flashdata('info'), 
							"info_type" => $this->session->flashdata('info_type'),
							"info_pesan" => $this->session->flashdata('info_pesan'),
							"user_id" => $user_id
						]);
				} else {
					$this->load->view('user/ubah_user', ["user_id" => $user_id]);
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function hapus(){
		if ($this->session->userdata('app-id') !== null) {
			$this->form_validation->set_rules('id', 'Kode', 'trim|required',
				array('required' => 'Kode harus diisi')
			);

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-danger");
				$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda.".validation_errors());
				header("Location: ".base_url()."user/master/");
				exit;
			} else {
				$simpan = $this->model_user->nonaktif(
					$_POST['id']
				);
				if ($simpan) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-success");
					$this->session->set_flashdata('info_pesan', "Hapus data berhasil.");
					header("Location: ".base_url()."user/master/");
					exit;
				} else {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Hapus data gagal.");
					header("Location: ".base_url()."user/master/");
					exit;
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}
	
}
