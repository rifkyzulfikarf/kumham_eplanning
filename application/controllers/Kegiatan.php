<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kegiatan extends CI_Controller {

	public $model_kegiatan = NULL;

	public function __construct() {
		parent::__construct();

		$this->load->model('Kegiatan_Model');
		$this->model_kegiatan = $this->Kegiatan_Model;
	}

	public function master() {
		if ($this->session->userdata('app-id') !== null) {
			$this->load->library('pagination');
			
			$keyword = strtoupper(urldecode($this->uri->segment(3, "all")));

			if ($keyword == "ALL") {
				$search = "%";
			} else {
				$search = $keyword;
			}
			$jumlah_data = $this->model_kegiatan->jumlah_data($search);

			$config['base_url'] = base_url().'kegiatan/master/'.$keyword.'/';
			$config['uri_segment'] = 4;
			$config['total_rows'] = $jumlah_data->jumlah;
			$config['per_page'] = 20;
			$config['num_links'] = 2;
			$config['first_link'] = '«';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = '»';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['prev_link'] = 'Prev';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li><a href='#'><b>";
			$config['cur_tag_close'] = '</b></a></li>';
			$config['next_link'] = 'Next';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$this->pagination->initialize($config);		

			$from = $this->uri->segment(4, 0);
			$data['kegiatans'] = $this->model_kegiatan->get_data($search, $config['per_page'], $from);
			$data['keyword'] = urldecode($keyword);

			if ($this->session->flashdata('info') !== null) {
				$data['info'] = $this->session->flashdata('info');
				$data['info_type'] = $this->session->flashdata('info_type');
				$data['info_pesan'] = $this->session->flashdata('info_pesan');
			}

			$this->load->view('kegiatan/mst_kegiatan',$data);
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function prep() {
		$loops = $this->db->query("SELECT kode_kegiatan, nama_kegiatan FROM kegiatan_temp;");
		foreach ($loops->result() as $loop) {
			$jumlah = $this->db->query("SELECT COUNT(id) AS jumlah FROM kegiatan_temp WHERE kode_kegiatan = '".$loop->kode_kegiatan."';")->row();
			if ($jumlah->jumlah != 0) {
				$simpan = $this->db->simple_query("INSERT INTO kegiatan VALUES('".$loop->kode_kegiatan."', '".$loop->nama_kegiatan."', '2018')");
				$delete = $this->db->simple_query("DELETE FROM kegiatan_temp WHERE kode_kegiatan = '".$loop->kode_kegiatan."';");
			}
		}
	}

	public function unggah_file() {
		if ($this->session->userdata('app-id') !== null) {
			$this->load->library('pagination');
			
			$keyword = strtoupper(urldecode($this->uri->segment(3, "all")));

			if ($keyword == "ALL") {
				$search = "%";
			} else {
				$search = $keyword;
			}
			$jumlah_data = $this->model_kegiatan->jumlah_data_file($search);

			$config['base_url'] = base_url().'kegiatan/unggah_file/'.$keyword.'/';
			$config['uri_segment'] = 4;
			$config['total_rows'] = $jumlah_data->jumlah;
			$config['per_page'] = 20;
			$config['num_links'] = 2;
			$config['first_link'] = '<<';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_link'] = '>>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['prev_link'] = '<';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['cur_tag_open'] = "<li><a href='#'><b>";
			$config['cur_tag_close'] = '</b></a></li>';
			$config['next_link'] = '>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$this->pagination->initialize($config);		

			$from = $this->uri->segment(4, 0);
			$data['files'] = $this->model_kegiatan->get_data_file($search, $config['per_page'], $from);
			$data['keyword'] = urldecode($keyword);

			if ($this->session->flashdata('info') !== null) {
				$data['info'] = $this->session->flashdata('info');
				$data['info_type'] = $this->session->flashdata('info_type');
				$data['info_pesan'] = $this->session->flashdata('info_pesan');
			}

			$this->load->view('kegiatan/unggah_file',$data);
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function unggah_file_dialog() {
		if ($this->session->userdata('app-id') !== null) {
			if (isset($_POST['btnSubmit'])) {
				
				$this->load->library('upload');

				$this->form_validation->set_rules('jenis', 'Jenis', 'trim|required');
				$this->form_validation->set_rules('id', 'ID', 'trim|required');
				if (empty($_FILES['file']['name'])) {
					$this->form_validation->set_rules('file', 'Document', 'required');
				}

				if ($this->form_validation->run() == FALSE) {
					$this->session->set_flashdata('info', true);
					$this->session->set_flashdata('info_type', "alert-danger");
					$this->session->set_flashdata('info_pesan', "Validasi form gagal. Cek kembali inputan anda : ".validation_errors());
					header("Location: ".base_url()."kegiatan/unggah_file_dialog/".$jenis."/".$id);
					exit;
				} else {

					$jenis = base64_decode($_POST['jenis']);
					$id = base64_decode($_POST['id']);

					$fileName = time().$_FILES['file']['name'];

					if ($jenis == '1') {
						$config['upload_path'] = './uploads/kak_tor_kegiatan/';
					} elseif ($jenis == '2') {
						$config['upload_path'] = './uploads/rab_kegiatan/';
					} elseif ($jenis == '3') {
						$config['upload_path'] = './uploads/misc_kegiatan/';
					}
         
			        
			        $config['file_name'] = $fileName;
			        $config['allowed_types'] = 'pdf';
			        $config['max_size'] = 60000;

			        $this->upload->initialize($config);

			        if(!$this->upload->do_upload('file')) {
		        	
			        	$this->session->set_flashdata('info', true);
						$this->session->set_flashdata('info_type', "alert-danger");
						$this->session->set_flashdata('info_pesan', "Upload gagal. ".$this->upload->display_errors());
						header("Location: ".base_url()."kegiatan/unggah_file_dialog/".$jenis."/".$id);
						exit;

			        } else {
			        	$simpan = $this->model_kegiatan->unggah_file_kegiatan(
							$id,
							$jenis,
							$fileName
							);

						if ($simpan) {
							$this->session->set_flashdata('info', true);
							$this->session->set_flashdata('info_type', "alert-success");
							$this->session->set_flashdata('info_pesan', "Simpan data berhasil.");
							header("Location: ".base_url()."kegiatan/unggah_file/");
							exit;
						} else {
							$this->session->set_flashdata('info', true);
							$this->session->set_flashdata('info_type', "alert-danger");
							$this->session->set_flashdata('info_pesan', "Simpan data gagal.");
							header("Location: ".base_url()."kegiatan/unggah_file_dialog/".$jenis."/".$id);
							exit;
						}
			        }
				}
			} else {
				$jenis = base64_encode($this->uri->segment(3, 0));
				$id = base64_encode($this->uri->segment(4, 0));
				if ($this->session->flashdata('info') !== null) {
					$this->load->view('kegiatan/unggah_file_dialog', 
						[
							"info" => $this->session->flashdata('info'), 
							"info_type" => $this->session->flashdata('info_type'),
							"info_pesan" => $this->session->flashdata('info_pesan'),
							"jenis" => $jenis,
							"id" => $id
						]);
				} else {
					$this->load->view('kegiatan/unggah_file_dialog', ["jenis"=>$jenis, "id"=>$id]);
				}
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function hapus_file_kegiatan() {
		if ($this->session->userdata('app-id') !== null) {
			$jenis = $this->uri->segment(3, 0);
			$id = $this->uri->segment(4, 0);
			$kegiatan = $this->model_kegiatan->get_file_kegiatan_by_id($id);

			if ($jenis == '1') {
				$path = FCPATH . './uploads/kak_tor_kegiatan/' . $kegiatan->file_kak_tor;
			} elseif ($jenis == '2') {
				$path = FCPATH . './uploads/rab_kegiatan/' . $kegiatan->file_rab;
			} elseif ($jenis == '3') {
				$path = FCPATH . './uploads/misc_kegiatan/' . $kegiatan->file_misc;
			}

			if (file_exists($path)) {   
				unlink($path);
			}

			$simpan = $this->model_kegiatan->hapus_file_kegiatan(
				$id, $jenis
			);
			if ($simpan) {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-success");
				$this->session->set_flashdata('info_pesan', "Hapus data berhasil.");
				header("Location: ".base_url()."kegiatan/unggah_file/");
				exit;
			} else {
				$this->session->set_flashdata('info', true);
				$this->session->set_flashdata('info_type', "alert-danger");
				$this->session->set_flashdata('info_pesan', "Hapus data gagal.");
				header("Location: ".base_url()."kegiatan/unggah_file/");
				exit;
			}
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');
		}
	}

	public function frontend() {
		if ($this->session->flashdata('info') !== null) {
			$data['info'] = $this->session->flashdata('info');
			$data['info_type'] = $this->session->flashdata('info_type');
			$data['info_pesan'] = $this->session->flashdata('info_pesan');
		}

		$data['files'] = $this->model_kegiatan->get_data_file('%', 1500, 0);

		$this->load->view('public/file_kegiatan',$data);
	}
	
}
